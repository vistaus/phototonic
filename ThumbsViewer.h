/*
 *  Copyright (C) 2013 Ofer Kashayov <oferkv@live.com>
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef THUMBS_VIEWER_H
#define THUMBS_VIEWER_H

#include <QtWidgets>
#include "Settings.h"

class Phototonic;
class MetadataCache;
class ImageViewer;
class ImageTags;
class InfoView;
class ImagePreview;

struct DirWrapper
{
    bool webmSupported = false;
    bool avifSupported = false;
    bool heicSupported = false;
    bool webpSupported = false;
    bool mp4Supported = false;
    bool icoSupported = false;

    DirWrapper() {
        const QList<QByteArray> supportedFormats = QImageReader::supportedImageFormats();
        if (supportedFormats.contains("webp")) {
            webpSupported = true;
        }
        if (supportedFormats.contains("avif")) {
            avifSupported = true;
        }
        if (supportedFormats.contains("heic")) {
            heicSupported = true;
        }
        if (supportedFormats.contains("ico")) {
            icoSupported = true;
        }

        if (supportedFormats.contains("webm")) {
            webmSupported = true;
            mp4Supported = true; // just an assumption for now, since qt-ffmpeg-imageplugin doesn't register mp4 as supported to avoid mucking up things
        }
    }

    static bool isWebM(const QByteArray &buffer) {
        if (buffer.size() < 79) {
            return false;
        }

        static const char *ebmlID = "\x1a\x45\xdf\xa3";
        if (!buffer.startsWith(ebmlID)) {
            return false;
        }

        static const char *docID = "\x42\x82";
        int index = buffer.indexOf(docID);
        if (index < 5 || index > 65) {
            return false;
        }

        static const char *webmID = "webm";
        index = buffer.indexOf(webmID);
        if (index >= 8 && index <= 75) {
            return true;
        }
        static const char *matroskaID = "matroska";
        index = buffer.indexOf(matroskaID);
        if (index >= 8 && index <= 75) {
            return true;
        }

        return false;
    }

#if defined(STARTS_WITH) || defined(STARTS_WITH_AT)
#error "define collision"
#endif
#define STARTS_WITH(x) (memcmp(rawBuffer, x, sizeof(x) - 1) == 0)
#define STARTS_WITH_AT(offset, x) (memcmp(rawBuffer + offset, x, sizeof(x) - 1) == 0)

    static constexpr int probeSize = 13;
    bool isImage(const QString &filePath) {
        QFile f(filePath);
        if (!f.open(QIODevice::ReadOnly)) {
            return false;
        }
        const QByteArray buffer = f.read(probeSize);
        if (buffer.length() < probeSize) {
            return false;
        }
        const char *rawBuffer = buffer.constData();

        // png
        static constexpr const char pngHeader[] = "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A";
        static_assert(probeSize >= sizeof(pngHeader));
        if (STARTS_WITH(pngHeader)) {
            return true;
        }
        // jpeg
        static constexpr const char jpegHeader[] = "\xff\xd8\xff";
        static_assert(probeSize >= sizeof(jpegHeader));
        if (STARTS_WITH(jpegHeader)) {
            return true;
        }

        // gif
        static constexpr const char gifHeader[] = "GIF8";
        static_assert(probeSize >= sizeof(gifHeader));
        if (STARTS_WITH(gifHeader)) {
            return true;
        }

        //webp
        static constexpr const char webpMagic[] = "WEBP";
        static_assert(probeSize >= sizeof(webpMagic) + 8);
        if (webpSupported && STARTS_WITH("RIFF") && memcmp(rawBuffer + 8, webpMagic, sizeof(webpMagic) - 1) == 0) {
            return true;
        }

        static constexpr const char tripleNull[] = "\0\0\0";
        static constexpr const char ftypMagic[] = "ftyp";
        static_assert(probeSize >= sizeof(tripleNull) + sizeof(ftypMagic) + 4);
        if ((heicSupported || mp4Supported) && STARTS_WITH(tripleNull) && STARTS_WITH_AT(4, ftypMagic)) {
            if (heicSupported) {
                static constexpr const char heicMagic[] = "hei";
                if (STARTS_WITH_AT(8, heicMagic)) {
                    return true;
                }
            }
            if (mp4Supported) {
                static constexpr const char isoMagic[] = "iso";
                if (STARTS_WITH_AT(8, isoMagic)) {
                    return true;
                }

                static constexpr const char mp4Magic[] = "mp4";
                if (STARTS_WITH_AT(8, mp4Magic)) {
                    return true;
                }

                static constexpr const char faceMagic[] = "FACE";
                if (STARTS_WITH_AT(8, faceMagic)) {
                    return true;
                }

                static constexpr const char quicktimeMagic[] = "qt  ";
                if (STARTS_WITH_AT(8, quicktimeMagic)) {
                    return true;
                }

                static constexpr const char m4aMagic[] = "M4A";
                if (STARTS_WITH_AT(8, m4aMagic)) {
                    return true;
                }

                static constexpr const char threeGpMagic[] = "3gp";
                if (STARTS_WITH_AT(8, threeGpMagic)) {
                    return true;
                }

                static constexpr const char avisMagic[] = "avis";
                if (STARTS_WITH_AT(8, avisMagic)) {
                    return true;
                }
            }
            if (avifSupported) {
                static constexpr const char avifMagic[] = "avif";
                if (STARTS_WITH_AT(8, avifMagic)) {
                    return true;
                }
            }
        }

        // webm
        if (webmSupported && isWebM(buffer)) {
            return true;
        }

        // bmp
        if (STARTS_WITH("BM")) {
            uint32_t sig;
            memcpy(&sig, rawBuffer + 14, sizeof sig);

            switch(sig) {
            case 16:
                qDebug() << "PC bitmap, OS/2 2.x format (DIB header size=16)";
                return true;
            case 24:
                qDebug() << "PC bitmap, OS/2 2.x format (DIB header size=24)";
                return true;
            case 48:
                qDebug() << "PC bitmap, OS/2 2.x format (DIB header size=48)";
                return true;
            case 64:
                qDebug() << "PC bitmap, OS/2 2.x format";
                return true;
            case 40:
                qDebug() << "PC bitmap, Windows 3.x format";
                return true;
            case 108:
                qDebug() << "PC bitmap, Windows 95/NT4 and newer format";
                return true;
            case 124:
                qDebug() << "PC bitmap, Windows 98/2000 and newer format";
                return true;
            case 52:
                qDebug() << "PC bitmap, Adobe Photoshop";
                return true;
            case 56:
                qDebug() << "PC bitmap, Adobe Photoshop with alpha channel mask";
                return true;

            // There are some more obscure ones, but I'm lazy
            default:
                break;
            }
        }
        // ico
        // offset 4 is number of frames
        static_assert(probeSize > 8);
        if (icoSupported && STARTS_WITH("\x0\x0\x1\x0") && rawBuffer[4] != 0) {
            return true;
        }

        return false;
    }
#undef STARTS_WITH
#undef STARTS_WITH_AT

    QFileInfoList entryInfoList() {
        if (!Settings::probeFiles && filterString.isEmpty()) {
            dir.setNameFilters(nameFilters);
            return dir.entryInfoList();
        }

        dir.setNameFilters(Settings::probeFiles ? QStringList() : nameFilters);
        QFileInfoList ret;
        for (const QFileInfo &info : dir.entryInfoList()) {
            if (!info.fileName().contains(filterString, Qt::CaseInsensitive)) { // empty filter string always matches
                continue;
            }

            if (suffixes.contains(info.suffix())) {
                ret.append(info);
                continue;
            }

            if (isImage(info.absoluteFilePath())) {
                ret.append(info);
            }
        }
        return ret;
    }
    void setPath(const QString &path) {
        dir.setPath(path);
    }
    void setNameFilters(const QStringList &filters) {
        nameFilters = filters;

        suffixes.clear();
        for (const QString &filter : filters) {
            if (filter.startsWith("*.") && filter.count('.') == 1) {
                suffixes.insert(filter.mid(2));
                continue;
            }
        }
    }
    QDir::Filters filter() const {
        return dir.filter();
    }
    void setFilter(const QDir::Filters filter) {
        dir.setFilter(filter);
    }
    void setSorting(const QDir::SortFlags sort) {
        dir.setSorting(sort);
    }

    QSet<QString> suffixes;
    QStringList nameFilters;
    QString filterString;
    QDir dir;
};

struct DuplicateImage
{
    QString filePath;
    int duplicates;
    int id = 0;
    qreal sortOrder = 0.;
};

struct Histogram
{
    float red[256]{};
    float green[256]{};
    float blue[256]{};

    static inline float compareChannel(const float hist1[256], const float hist2[256])
    {
        float len1 = 0.f, len2 = 0.f, corr = 0.f;

        for (uint16_t i=0; i<256; i++) {
            len1 += hist1[i];
            len2 += hist2[i];
            corr += std::sqrt(hist1[i] * hist2[i]);
        }

        const float part1 = 1.f / std::sqrt(len1 * len2);

        return std::sqrt(1.f - part1 * corr);
    }

    inline float compare(const Histogram &other) const
    {
        return compareChannel(red, other.red) +
            compareChannel(green, other.green) +
            compareChannel(blue, other.blue);
    }

    bool load(const QImage &input) {
        if (input.isNull()) {
            return false;
        }
        QImage image = input;
        switch(input.format()) {
        case QImage::Format_ARGB32:
        case QImage::Format_RGB32:
            break;
        case QImage::Format_ARGB32_Premultiplied:
        default:
            image = image.convertToFormat(QImage::Format_RGB32);
            break;
        }

        bzero(red, sizeof red);
        bzero(green, sizeof green);
        bzero(blue, sizeof blue);
        const int width = image.width();
        const int height = image.height();
        for (int y=0; y<height; y++) {
            const uchar *line = image.constScanLine(y);
            for (int x=0; x<width*4; x += 4) {
                red  [line[x + 0]] += 1.f;
                green[line[x + 1]] += 1.f;
                blue [line[x + 2]] += 1.f;
            }
        }
        return true;
    }

    QPixmap render(const QSize &size) const {
        QPointF redPoly[258];
        QPointF greenPoly[258];
        QPointF bluePoly[258];

        float maxY = 0;
        maxY = 0;
        for (int i=0; i<256; i++) {
            maxY = qMax(red[i], qMax(green[i], qMax(blue[i], maxY)));
        }

        const qreal height = size.height();
        const qreal width = size.width();

        // Start and end points
        redPoly[0] = greenPoly[0] = bluePoly[0] = QPointF(0, height);
        redPoly[257] = greenPoly[257] = bluePoly[257] = QPointF(width, height);

        const qreal multY = (height - 5) / maxY;

        QPointF *rp = &redPoly[1];
        QPointF *gp = &greenPoly[1];
        QPointF *bp = &bluePoly[1];

        for (int i=0; i<256; i++) {
            const qreal x = i * width / 256.;
            rp[i] = QPointF(x, height - red[i] * multY);
            gp[i] = QPointF(x, height - green[i] * multY);
            bp[i] = QPointF(x, height - blue[i] * multY);
        }

        QPixmap rendered(size);
        rendered.fill(Qt::black);

        QPainter p(&rendered);
        p.setRenderHint(QPainter::Antialiasing);

        // Plain blue is barely visible
        p.setPen(QColor(0, 128, 255));
        p.setBrush(QColor(0, 100, 255, 100));
        p.drawConvexPolygon(bluePoly, 258);

        p.setPen(Qt::red);
        p.setBrush(QColor(255, 0, 0, 64));
        p.drawConvexPolygon(redPoly, 258);

        p.setPen(Qt::green);
        p.setBrush(QColor(0, 255, 0, 64));
        p.drawConvexPolygon(greenPoly, 258);


        p.end();

        return rendered;
    }
};
Q_DECLARE_METATYPE(Histogram);

class ThumbsViewer : public QListView {
Q_OBJECT

public:
    enum UserRoles {
        FileNameRole = Qt::UserRole + 1,
        FileNameSortRole = Qt::UserRole + 1,
        SortRole,
        LoadedRole,
        BrightnessRole,
        TypeRole,
        SizeRole,
        TimeRole,
        SimilarityRole,
        DuplicateOrder,
        ResolutionRole,
        RoleCount
    };
    enum ThumbnailLayouts {
        Classic,
        Squares,
        Compact
    };
    enum SimilarityMode {
        ColorSimilarity,
        ContentSimilarity
    };

    ThumbsViewer(QWidget *parent, const std::shared_ptr<MetadataCache> &metadataCache);

    void updateItemLayout();

    void applyFilter();

    void reLoad();

    void loadDuplicates();

    void loadFileList();

    void loadSubDirectories();

    void setThumbColors();

    bool setCurrentIndexByName(QString &fileName);

    bool setCurrentIndexByRow(int row);

    void setCurrentRow(int row);

    void setImageViewerWindowTitle();

    void setNeedToScroll(bool needToScroll);

    void selectCurrentIndex();

    QStandardItem *addThumb(const QString &imageFullPath, qreal sortValue = -1);

    void abort(bool permanent = false);

    void selectThumbByRow(int row);

    void selectByBrightness(qreal min, qreal max);

    int getNextRow();

    int getPrevRow();

    int getLastRow();

    int getRandomRow();

    int getCurrentRow();

    QStringList getSelectedThumbsList();

    QString getSingleSelectionFilename();

    void setImageViewer(ImageViewer *imageViewer);

    void sortBySimilarity(const SimilarityMode mode);

    void refreshThumbnails(bool clearContents);

    InfoView *infoView;
    ImagePreview *imagePreview;
    ImageTags *imageTags;
    DirWrapper thumbsDir;
    QStringList fileFilters;
    QStandardItemModel *thumbsViewerModel;
    QDir::SortFlags thumbsSortFlags;
    int thumbSize;
    bool isBusy;

protected:
    void startDrag(Qt::DropActions) override;
    void mousePressEvent(QMouseEvent *event) override;

    // There should be a signal for this
    void resizeEvent(QResizeEvent *event) override {
        QListView::resizeEvent(event);
        updateVisibleThumbnails();
    }

private:
    const QPixmap &getErrorPixmap();

    void analyzeImage(const QString &filename, const QImage &image);

    QImage loadThumbImage(const QString &filename, const int targetSize, const bool shrinkable);

    void initThumbs();

    bool loadThumb(int row);

    void resetModel();

    void findDupes(bool resetCounters);

    int getFirstVisibleThumb();

    int getLastVisibleThumb();

    void updateThumbsCount(const bool scanning = false);

    void updateFoundDupesState(int duplicates, int filesScanned, int originalImages);

    void updateImageInfoViewer(int row);

    QSize itemSizeHint() const;

    void ensureHistogramsAndHashes(QProgressDialog *progress);

    void loadVisibleThumbs(int scrollBarValue = 0);

    QString thumbnailFileName(const QString &path) const;
    QString locateThumbnail(const QString &path, const QSize &targetSize, const bool shrinkable) const;
    void storeThumbnail(const QString &originalPath, QImage thumbnail, const QSize &originalSize) const;

    QFileInfo thumbFileInfo;
    QFileInfoList thumbFileInfoList;

    QList<Histogram> histograms;
    QList<QString> sortedFiles;
    QList<uint64_t> imageHashes;
    QSet<QString> loadedSortedFiles;
    QSet<QString> failedFiles;

    QHash<QString, int> nameSortValues;

    uint64_t rootHash;
    Histogram rootHist;
    int rootHashIndex = 0;
    int rootHistIndex = 0;

    QPixmap errorPixmap;
    QPixmap emptyImg;
    QModelIndex m_currentIndex;
    Phototonic *phototonic;
    std::shared_ptr<MetadataCache> metadataCache;
    ImageViewer *imageViewer;
    QHash<uint64_t, DuplicateImage> dupImageHashes;
    bool isAbortThumbsLoading = false;
    bool isClosing = false;
    bool isNeedToScroll = false;
    int currentRow = 0;
    bool scrolledForward = false;
    int thumbsRangeFirst;
    int thumbsRangeLast;

    bool reloadVisible = false;

    QTimer m_selectionChangedTimer;
    QTimer m_loadThumbTimer;
    QTimer m_loadVisibleTimer;

public slots:
    void onOrderingChanged() {
        reloadVisible = true;
        updateVisibleThumbnails();
    }

    void updateVisibleThumbnails() {
        if (!m_loadVisibleTimer.isActive()) {
            m_loadVisibleTimer.start();
        }
    }

    void onSelectionChanged();

    void invertSelection();

private slots:

    void loadThumbsRange();

    void loadAllThumbs();
};

#endif // THUMBS_VIEWER_H

