/*
 *  Copyright (C) 2013-2014 Ofer Kashayov - oferkv@live.com
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CropRubberband.h"
#include "Settings.h"

#define GRIPSIZE 20

CropRubberBand::CropRubberBand(QWidget *parent) : QWidget(parent) {

    setWindowFlags(Qt::SubWindow);
    setMouseTracking(true);

    setFocusPolicy(Qt::ClickFocus);

    setCursor(Qt::OpenHandCursor);
    setMinimumSize(GRIPSIZE*2, GRIPSIZE*2);
}

void CropRubberBand::showEvent(QShowEvent *) {
    setFocus();
    emit selectionChanged(geometry());
}

void CropRubberBand::keyPressEvent(QKeyEvent *event) {
    QPoint cursorPosGlobal = QCursor::pos();
    QPoint cursorPos = mapFromGlobal(cursorPosGlobal);
    QRect geom = geometry();

    switch (event->key()) {
    case Qt::LeftArrow:
    case Qt::Key_H:
        if (cursorPos.x() > -10 && cursorPos.x() < 10) {
            geom.setLeft(geom.left() - 1);
            QCursor::setPos(cursorPosGlobal.x() - 1, cursorPosGlobal.y());
        } else if (cursorPos.x() > width() - 10 && cursorPos.x() < width() + 10) {
            geom.setRight(geom.right() - 1);
            QCursor::setPos(cursorPosGlobal.x() - 1, cursorPosGlobal.y());
        } else if (QRect(geom).translated(-geom.x(), -geom.y()).contains(cursorPos)) {
            geom.moveLeft(geom.x() - 1);
        }
        setGeometry(geom);
        emit selectionChanged(geom);
        event->accept();
        break;
    case Qt::DownArrow:
    case Qt::Key_J:
        if (cursorPos.y() > -10 && cursorPos.y() < 10) {
            geom.setTop(geom.top() + 1);
            QCursor::setPos(cursorPosGlobal.x(), cursorPosGlobal.y() + 1);
        } else if (cursorPos.y() > height() - 10 && cursorPos.y() < height() + 10) {
            geom.setBottom(geom.bottom() + 1);
            QCursor::setPos(cursorPosGlobal.x(), cursorPosGlobal.y() + 1);
        } else if (QRect(geom).translated(-geom.x(), -geom.y()).contains(cursorPos)) {
            geom.moveTop(geom.top() + 1);
        }
        setGeometry(geom);
        emit selectionChanged(geom);
        event->accept();
        break;
    case Qt::UpArrow:
    case Qt::Key_K:
        if (cursorPos.y() > -10 && cursorPos.y() < 10) {
            geom.setTop(geom.top() - 1);
            QCursor::setPos(cursorPosGlobal.x(), cursorPosGlobal.y() - 1);
        } else if (cursorPos.y() > height() - 10 && cursorPos.y() < height() + 10) {
            geom.setBottom(geom.bottom() - 1);
            QCursor::setPos(cursorPosGlobal.x(), cursorPosGlobal.y() - 1);
        } else if (QRect(geom).translated(-geom.x(), -geom.y()).contains(cursorPos)) {
            geom.moveTop(geom.top() - 1);
        }
        setGeometry(geom);
        emit selectionChanged(geom);
        event->accept();
        break;
    case Qt::RightArrow:
    case Qt::Key_L:
        if (cursorPos.x() > -10 && cursorPos.x() < 10) {
            geom.setLeft(geom.left() + 1);
            QCursor::setPos(cursorPosGlobal.x() + 1, cursorPosGlobal.y());
        } else if (cursorPos.x() > width() - 10 && cursorPos.x() < width() + 10) {
            geom.setRight(geom.right() + 1);
            QCursor::setPos(cursorPosGlobal.x() + 1, cursorPosGlobal.y());
        } else if (QRect(geom).translated(-geom.x(), -geom.y()).contains(cursorPos)) {
            geom.moveLeft(geom.x() + 1);
        }
        setGeometry(geom);
        emit selectionChanged(geom);
        event->accept();
        break;
    }
}

void CropRubberBand::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setPen(QColor(255, 255, 255, 192));
    p.setBrush(QColor(128, 128, 128, 128));

    // GRIPSIZE * 2 because the pie is approx. half size visually
    QRect corner(-GRIPSIZE, -GRIPSIZE, GRIPSIZE*2, GRIPSIZE*2);
    p.drawPie(corner, 270*16, 90*16);
    corner.moveTo(width() - corner.width() - 1 + GRIPSIZE, -GRIPSIZE);
    p.drawPie(corner, 180*16, 90*16);
    corner.moveTo(width() - corner.width() - 1 + GRIPSIZE, height() - corner.height() - 1 + GRIPSIZE);
    p.drawPie(corner, 90*16, 90*16);
    corner.moveTo(-GRIPSIZE, height() - corner.height() - 1 + GRIPSIZE);
    p.drawPie(corner, 0*16, 90*16);


    p.setBrush(Qt::transparent);
    p.drawRect(0, 0, width() - 1, height() - 1);
}

QRect CropRubberBand::cornerRect(Qt::Corner corner) const {
    switch(corner) {
    case Qt::TopLeftCorner:
        return QRect(0, 0, GRIPSIZE, GRIPSIZE);
    case Qt::TopRightCorner:
        return QRect(width() - GRIPSIZE, 0, GRIPSIZE, GRIPSIZE);
    case Qt::BottomLeftCorner:
        return QRect(0, height() - GRIPSIZE, GRIPSIZE, GRIPSIZE);
    case Qt::BottomRightCorner:
        return QRect(width() - GRIPSIZE, height() - GRIPSIZE, GRIPSIZE, GRIPSIZE);
    default:
        return rect();
    }
}

void CropRubberBand::resizeEvent(QResizeEvent *) {
    emit selectionChanged(geometry());
}

void CropRubberBand::mousePressEvent(QMouseEvent *event)
{
    prevPos = event->globalPos();

    if (cornerRect(Qt::TopLeftCorner).contains(event->pos())) {
        m_pressedHandle = Qt::TopLeftCorner;
        cropOrigin = geometry().bottomRight();
    } else if (cornerRect(Qt::TopRightCorner).contains(event->pos())) {
        m_pressedHandle = Qt::TopRightCorner;
        cropOrigin = geometry().bottomLeft();
    } else if (cornerRect(Qt::BottomLeftCorner).contains(event->pos())) {
        m_pressedHandle = Qt::BottomLeftCorner;
        cropOrigin = geometry().topRight();
    } else if (cornerRect(Qt::BottomRightCorner).contains(event->pos())) {
        m_pressedHandle = Qt::BottomRightCorner;
        cropOrigin = geometry().topLeft();
    } else {
        m_pressedHandle = -1;
        cropOrigin = QPoint(-1, -1);
        return;
    }
}

void CropRubberBand::setBounds(const QRect &bounds) {
    m_bounds = bounds;
    // TODO: need to preserve squareness
    //setGeometry(geometry() & bounds);
}

void CropRubberBand::mouseReleaseEvent(QMouseEvent *) {
    m_pressedHandle = -1;
    cropOrigin = QPoint();
}

QPoint CropRubberBand::boundedPoint(QPoint point, const QSize &size) const
{
    if (!m_bounds.isValid()) {
        return point;
    }
    if (size.width() > m_bounds.width() || size.height() > m_bounds.height()) {
        qDebug() << "Invalid size" << size << m_bounds.size();
        return point;
    }
    if (point.x() < m_bounds.x()) {
        point.setX(m_bounds.x());
    } else if (point.x() + size.width() > m_bounds.right()) {
        point.setX(m_bounds.right() - size.width());
    }
    if (point.y() < m_bounds.y()) {
        point.setY(m_bounds.y());
    } else if (point.y() + size.height() > m_bounds.bottom() + 1) {
        point.setY(m_bounds.bottom() - size.height() + 1);
    }
    return point;
}

void CropRubberBand::mouseMoveEvent(QMouseEvent *event)
{
    if (cornerRect(Qt::TopLeftCorner).contains(event->pos()) || cornerRect(Qt::BottomRightCorner).contains(event->pos())) {
        setCursor(Qt::SizeFDiagCursor);
    } else if (cornerRect(Qt::TopRightCorner).contains(event->pos()) || cornerRect(Qt::BottomLeftCorner).contains(event->pos())) {
        setCursor(Qt::SizeBDiagCursor);
    } else {
        setCursor(event->buttons() ? Qt::ClosedHandCursor : Qt::OpenHandCursor);
    }
    if (!event->buttons()) {
        return;
    }

    if (m_pressedHandle == -1 || cropOrigin.x() < 0 || cropOrigin.y() < 0) {
        move(boundedPoint(pos() + (event->globalPos() - prevPos), size()));
        prevPos = event->globalPos();
        return;
    }

    QPoint eventPos = boundedPoint(event->pos() + pos());

    int deltaX = cropOrigin.x() - eventPos.x();
    if (qAbs(deltaX) < minimumWidth()) {
        deltaX = minimumWidth() * (deltaX < 0 ? -1 : 1);
    }
    int deltaY = cropOrigin.y() - eventPos.y();
    if (qAbs(deltaY) < minimumHeight()) {
        deltaY = minimumWidth() * (deltaY < 0 ? -1 : 1);
    }

    QSize size;

    // Force square
    if ((event->modifiers() & Qt::ShiftModifier) || Settings::squareCrop) {
        size = QSize(-deltaX, deltaY < 0 ? qAbs(deltaX) : -qAbs(deltaX));
    } else {
        size = QSize(-deltaX, -deltaY);
    }

    setGeometry(QRect(cropOrigin, size).normalized());
}

void CropRubberBand::moveEvent(QMoveEvent *) {
    emit selectionChanged(geometry());
}
