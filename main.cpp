/*
 *  Copyright (C) 2013-2015 Ofer Kashayov <oferkv@live.com>
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Phototonic.h"
#include <QApplication>
#include <QCommandLineParser>
#include <QLoggingCategory>

#ifdef Q_OS_UNIX
#include <unistd.h>
#endif

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(context);
    // The only way to shut up the official Qt image plugins
    static const QSet<QString> blacklist = {
        "Invalid SOS parameters for sequential JPEG",
        "Invalid JPEG file structure: two SOI markers",
        "Bogus marker length",
        "Bogus Huffman table definition",
        "libpng warning: iCCP: known incorrect sRGB profile"
    };
    if (blacklist.contains(msg)) {
        return;
    }
    static const QStringList startBlacklist = {
        "Corrupt JPEG data:",
        "Unsupported marker type ",
        "Unsupported JPEG process: SOF type",
        "Invalid JPEG file structure: ",
        "Bogus DAC index"
    };
    for (const QString &blacklisted : startBlacklist) {
        if (msg.startsWith(blacklisted)) {
            return;
        }
    }

    QByteArray typeText;
    QByteArray typeColor;
    switch (type) {
    case QtDebugMsg:
        typeColor = QByteArrayLiteral("\033[02;32m");
        typeText = QByteArrayLiteral("Debug");
        break;
    case QtInfoMsg:
        typeColor = QByteArrayLiteral("\033[01;32m");
        typeText = QByteArrayLiteral("Info");
        break;
    case QtWarningMsg:
        typeColor = QByteArrayLiteral("\033[01;33m");
        typeText = QByteArrayLiteral("Warning");
        break;
    case QtCriticalMsg:
        typeColor = QByteArrayLiteral("\033[01;31m");
        typeText = QByteArrayLiteral("Critical");
        break;
    case QtFatalMsg:
        typeColor = QByteArrayLiteral("\033[01;31m");
        typeText = QByteArrayLiteral("Fatal");
        break;
    }
    QByteArray output;
    QByteArray endColor;
#ifdef Q_OS_UNIX
    if (isatty(STDOUT_FILENO)) {
        output += typeColor;
        endColor = QByteArrayLiteral("\033[0m");
    }
#endif
    output += typeText + endColor + " " + msg.toLocal8Bit();
    puts(output.constData());
}

int main(int argc, char *argv[]) {
    QApplication QApp(argc, argv);
    QLocale locale = QLocale::system();
    QCoreApplication::setApplicationVersion(VERSION);
#ifdef Q_OS_UNIX
    setvbuf(stdout, nullptr, _IOLBF, 0);
#endif
    qInstallMessageHandler(messageHandler);

    QLoggingCategory::setFilterRules(QStringLiteral("qt.gui.icc=false"));

    QCommandLineParser parser;
    parser.setApplicationDescription(VERSION " image viewer.");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument(QCoreApplication::translate("main", "files or directory"),
                                 QCoreApplication::translate("main", "files or directory to open"),
                                 QCoreApplication::translate("main", "[FILE...] | [DIRECTORY]"));

    QCommandLineOption langOption(QStringList() << "l" << "lang",
                                             QCoreApplication::translate("main", "start with a specific translation"),
                                             QCoreApplication::translate("main", "language"));
    parser.addOption(langOption);

    QCommandLineOption targetDirectoryOption(QStringList() << "o" << "output-directory",
            QCoreApplication::translate("main", "Copy all modified images into <directory>."),
            QCoreApplication::translate("main", "directory"));
    parser.addOption(targetDirectoryOption);

    parser.process(QApp);

    if (parser.isSet(langOption))
        locale = QLocale(parser.value(langOption));

    QTranslator qTranslator;
    qTranslator.load(locale, "qt", "_", QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    QApp.installTranslator(&qTranslator);

    QTranslator qTranslatorPhototonic;
    qTranslatorPhototonic.load(locale, "phototonic", "_", ":/translations");
    QApp.installTranslator(&qTranslatorPhototonic);

    Phototonic phototonic(parser.positionalArguments(), 0);
    if (parser.isSet(targetDirectoryOption))
        phototonic.setSaveDirectory(parser.value(targetDirectoryOption));
    phototonic.show();
    return QApp.exec();
}
