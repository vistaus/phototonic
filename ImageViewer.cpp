/*
 *  Copyright (C) 2013-2014 Ofer Kashayov - oferkv@live.com
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ImageViewer.h"
#include "Exiv.h"
#include "Phototonic.h"
#include "MessageBox.h"
#include "rgbhsv.h"
#include "Settings.h"
#include "ImageWidget.h"
#include "CropRubberband.h"
#include "MetadataCache.h"

#include <QThreadPool>
#include <QSemaphore>

#include <QElapsedTimer>

#define CLIPBOARD_IMAGE_NAME "clipboard.png"

namespace { // anonymous, not visible outside of this file
Q_DECLARE_LOGGING_CATEGORY(PHOTOTONIC_EXIV2_LOG)
Q_LOGGING_CATEGORY(PHOTOTONIC_EXIV2_LOG, "phototonic.exif", QtCriticalMsg)

struct Exiv2LogHandler {
    static void handleMessage(int level, const char *message) {
        switch(level) {
            case Exiv2::LogMsg::debug:
                qCDebug(PHOTOTONIC_EXIV2_LOG) << message;
                break;
            case Exiv2::LogMsg::info:
                qCInfo(PHOTOTONIC_EXIV2_LOG) << message;
                break;
            case Exiv2::LogMsg::warn:
            case Exiv2::LogMsg::error:
            case Exiv2::LogMsg::mute:
                qCWarning(PHOTOTONIC_EXIV2_LOG) << message;
                break;
            default:
                qCWarning(PHOTOTONIC_EXIV2_LOG) << "unhandled log level" << level << message;
                break;
        }
    }

    Exiv2LogHandler() {
        Exiv2::LogMsg::setHandler(&Exiv2LogHandler::handleMessage);
    }
};

class MyScrollArea : public QScrollArea {
protected:
    void wheelEvent(QWheelEvent *event) override {
        event->ignore();
        return;
    }
};

} // anonymous namespace


ImageViewer::ImageViewer(QWidget *parent, const std::shared_ptr<MetadataCache> &metadataCache) : QWidget(parent) {
    // This is a threadsafe way to ensure that we only register it once
    static Exiv2LogHandler handler;

    this->phototonic = (Phototonic *) parent;
    this->metadataCache = metadataCache;
    cursorIsHidden = false;
    moveImageLocked = false;
    mirrorLayout = LayNone;
    imageWidget = new ImageWidget;

    setBackgroundColor();

    QVBoxLayout *scrollLayout = new QVBoxLayout;
    scrollLayout->setContentsMargins(0, 0, 0, 0);
    scrollLayout->setSpacing(0);
    scrollLayout->addWidget(imageWidget);
    this->setLayout(scrollLayout);

    imageInfoLabel = new QLabel(this);
    imageInfoLabel->setVisible(Settings::showImageName);
    imageInfoLabel->setMargin(3);
    imageInfoLabel->move(10, 10);
    imageInfoLabel->setStyleSheet("QLabel { background-color : black; color : white; border-radius: 3px} ");

    feedbackLabel = new QLabel(this);
    feedbackLabel->setVisible(false);
    feedbackLabel->setMargin(3);
    feedbackLabel->setStyleSheet("QLabel { background-color : black; color : white; border-radius: 3px} ");

    QGraphicsOpacityEffect *infoEffect = new QGraphicsOpacityEffect;
    infoEffect->setOpacity(0.5);
    imageInfoLabel->setGraphicsEffect(infoEffect);
    QGraphicsOpacityEffect *feedbackEffect = new QGraphicsOpacityEffect;
    feedbackEffect->setOpacity(0.5);
    feedbackLabel->setGraphicsEffect(feedbackEffect);

    mouseMovementTimer = new QTimer(this);
    connect(mouseMovementTimer, SIGNAL(timeout()), this, SLOT(monitorCursorState()));

    Settings::cropLeft = Settings::cropTop = Settings::cropWidth = Settings::cropHeight = 0;
    Settings::cropLeftPercent = Settings::cropTopPercent = Settings::cropWidthPercent = Settings::cropHeightPercent = 0;

    Settings::hueVal = 0;
    Settings::saturationVal = 100;
    Settings::lightnessVal = 100;
    Settings::hueRedChannel = true;
    Settings::hueGreenChannel = true;
    Settings::hueBlueChannel = true;

    Settings::contrastVal = 78;
    Settings::brightVal = 100;

    Settings::dialogLastX = Settings::dialogLastY = 0;

    Settings::mouseRotateEnabled = false;

    newImage = false;

    cropRubberBand = new CropRubberBand(this);
    cropRubberBand->hide();
    connect(cropRubberBand, &CropRubberBand::selectionChanged,
            this, &ImageViewer::updateRubberBandFeedback);
}

static unsigned int getHeightByWidth(int imgWidth, int imgHeight, int newWidth) {
    float aspect;
    aspect = (float) imgWidth / (float) newWidth;
    return (imgHeight / aspect);
}

static unsigned int getWidthByHeight(int imgHeight, int imgWidth, int newHeight) {
    float aspect;
    aspect = (float) imgHeight / (float) newHeight;
    return (imgWidth / aspect);
}

qreal ImageViewer::scaledZoom() const {
    const qreal imageWidth = imageWidget->imageSize().width();
    const qreal viewWidth = imageWidget->viewRect().width();
    if (qFuzzyIsNull(imageWidth) || qFuzzyIsNull(viewWidth)) {
        return Settings::imageZoomFactor;
    }
    return Settings::imageZoomFactor * viewWidth / imageWidth;
}

static inline int calcZoom(int size) {
    return size * Settings::imageZoomFactor;
}

void ImageViewer::resizeImage(const QPointF &centerPoint) {
    static bool busy = false;
    if (busy) {
        return;
    }
    QSizeF imageSize;
    if (imageWidget) {
        imageSize = imageWidget->imageSize();
    } else {
        return;
    }
    if (imageSize.isEmpty()) {
        return;
    }

    busy = true;

    int imageViewWidth = this->size().width();
    int imageViewHeight = this->size().height();

    if (tempDisableResize) {
        imageSize.scale(imageSize.width(), imageSize.height(), Qt::KeepAspectRatio);
    } else {
        switch (Settings::zoomInFlags) {
            case Disable:
                if (imageSize.width() <= imageViewWidth && imageSize.height() <= imageViewHeight) {
                    imageSize.scale(calcZoom(imageSize.width()),
                                    calcZoom(imageSize.height()),
                                    Qt::KeepAspectRatio);
                }
                break;

            case WidthAndHeight:
                if (imageSize.width() <= imageViewWidth && imageSize.height() <= imageViewHeight) {
                    imageSize.scale(calcZoom(imageViewWidth),
                                    calcZoom(imageViewHeight),
                                    Qt::KeepAspectRatio);
                }
                break;

            case Width:
                if (imageSize.width() <= imageViewWidth) {
                    imageSize.scale(calcZoom(imageViewWidth),
                                    calcZoom(getHeightByWidth(imageSize.width(),
                                                              imageSize.height(),
                                                              imageViewWidth)),
                                    Qt::KeepAspectRatio);
                }
                break;

            case Height:
                if (imageSize.height() <= imageViewHeight) {
                    imageSize.scale(calcZoom(getWidthByHeight(imageSize.height(),
                                                              imageSize.width(),
                                                              imageViewHeight)),
                                    calcZoom(imageViewHeight),
                                    Qt::KeepAspectRatio);
                }
                break;

            case Disprop:
                int newWidth = imageSize.width(), newHeight = imageSize.height();
                if (newWidth <= imageViewWidth) {
                    newWidth = imageViewWidth;
                }
                if (newHeight <= imageViewHeight) {
                    newHeight = imageViewHeight;
                }
                imageSize.scale(calcZoom(newWidth), calcZoom(newHeight), Qt::IgnoreAspectRatio);
                break;
        }

        switch (Settings::zoomOutFlags) {
            case Disable:
                if (imageSize.width() >= imageViewWidth || imageSize.height() >= imageViewHeight) {
                    imageSize.scale(calcZoom(imageSize.width()),
                                    calcZoom(imageSize.height()),
                                    Qt::KeepAspectRatio);
                }
                break;

            case WidthAndHeight:
                if (imageSize.width() >= imageViewWidth || imageSize.height() >= imageViewHeight) {
                    imageSize.scale(calcZoom(imageViewWidth),
                                    calcZoom(imageViewHeight),
                                    Qt::KeepAspectRatio);
                }
                break;

            case Width:
                if (imageSize.width() >= imageViewWidth) {
                    imageSize.scale(calcZoom(imageViewWidth),
                                    calcZoom(getHeightByWidth(imageSize.width(),
                                                              imageSize.height(),
                                                              imageViewWidth)),
                                    Qt::KeepAspectRatio);
                }
                break;

            case Height:
                if (imageSize.height() >= imageViewHeight) {
                    imageSize.scale(calcZoom(getWidthByHeight(imageSize.height(),
                                                              imageSize.width(),
                                                              imageViewHeight)),
                                    calcZoom(imageViewHeight),
                                    Qt::KeepAspectRatio);
                }
                break;

            case Disprop:
                int newWidth = imageSize.width(), newHeight = imageSize.height();
                if (newWidth >= imageViewWidth) {
                    newWidth = imageViewWidth;
                }
                if (newHeight >= imageViewHeight) {
                    newHeight = imageViewHeight;
                }
                imageSize.scale(calcZoom(newWidth), calcZoom(newHeight), Qt::IgnoreAspectRatio);
                break;
        }
    }

    if (centerPoint.x() >= 0 && centerPoint.y() >= 0) {
        imageWidget->setViewSize(imageSize, centerPoint);
    } else {
        imageWidget->setViewSize(imageSize, imageWidget->rect().center());
    }

    if (qFuzzyCompare(Settings::imageZoomFactor, 1.f)) {
        imageWidget->centerImage();
    }
    if (cropRubberBand) {
        QRect imageRect(QPoint(0, 0), imageWidget->imageSize());
        QRect bounds(imageWidget->mapFromImage(imageRect.topLeft()), imageWidget->mapFromImage(imageRect.bottomRight()));
        cropRubberBand->setBounds(bounds);
    }

    busy = false;
}

void ImageViewer::resizeEvent(QResizeEvent *event) {
    QWidget::resizeEvent(event);
    resizeImage();
}

void ImageViewer::showEvent(QShowEvent *event) {
    QWidget::showEvent(event);
    resizeImage();
}

void ImageViewer::rotateByExifRotation(QImage &image, const QString &imageFullPath) {
    QTransform trans;
    long orientation = metadataCache->getImageOrientation(imageFullPath);

    switch (orientation) {
        case 1:
            break;
        case 2:
            image = image.mirrored(true, false);
            break;
        case 3:
            trans.rotate(180);
            image = image.transformed(trans, Qt::SmoothTransformation);
            break;
        case 4:
            image = image.mirrored(false, true);
            break;
        case 5:
            trans.rotate(90);
            image = image.transformed(trans, Qt::SmoothTransformation);
            image = image.mirrored(true, false);
            break;
        case 6:
            trans.rotate(90);
            image = image.transformed(trans, Qt::SmoothTransformation);
            break;
        case 7:
            trans.rotate(90);
            image = image.transformed(trans, Qt::SmoothTransformation);
            image = image.mirrored(false, true);
            break;
        case 8:
            trans.rotate(270);
            image = image.transformed(trans, Qt::SmoothTransformation);
            break;
        default:
            break;
    }
}

void ImageViewer::transform() {
    if (!qFuzzyCompare(Settings::rotation, 0)) {
        QTransform trans;
        trans.rotate(Settings::rotation);
        viewerImage = viewerImage.transformed(trans, Qt::SmoothTransformation);
    }

    if (Settings::flipH || Settings::flipV) {
        viewerImage = viewerImage.mirrored(Settings::flipH, Settings::flipV);
    }

    int cropLeftPercentPixels = 0, cropTopPercentPixels = 0, cropWidthPercentPixels = 0, cropHeightPercentPixels = 0;
    bool croppingOn = false;
    if (Settings::cropLeftPercent || Settings::cropTopPercent
        || Settings::cropWidthPercent || Settings::cropHeightPercent) {
        croppingOn = true;
        cropLeftPercentPixels = (viewerImage.width() * Settings::cropLeftPercent) / 100;
        cropTopPercentPixels = (viewerImage.height() * Settings::cropTopPercent) / 100;
        cropWidthPercentPixels = (viewerImage.width() * Settings::cropWidthPercent) / 100;
        cropHeightPercentPixels = (viewerImage.height() * Settings::cropHeightPercent) / 100;
    }

    if (Settings::cropLeft || Settings::cropTop || Settings::cropWidth || Settings::cropHeight) {
        viewerImage = viewerImage.copy(
                Settings::cropLeft + cropLeftPercentPixels,
                Settings::cropTop + cropTopPercentPixels,
                viewerImage.width() - Settings::cropLeft - Settings::cropWidth - cropLeftPercentPixels -
                cropWidthPercentPixels,
                viewerImage.height() - Settings::cropTop - Settings::cropHeight - cropTopPercentPixels -
                cropHeightPercentPixels);
    } else {
        if (croppingOn) {
            viewerImage = viewerImage.copy(
                    cropLeftPercentPixels,
                    cropTopPercentPixels,
                    viewerImage.width() - cropLeftPercentPixels - cropWidthPercentPixels,
                    viewerImage.height() - cropTopPercentPixels - cropHeightPercentPixels);
        }
    }
}

void ImageViewer::mirror() {
    switch (mirrorLayout) {
        case LayDual: {
            mirrorImage = QImage(viewerImage.width() * 2, viewerImage.height(),
                                 QImage::Format_ARGB32);
            QPainter painter(&mirrorImage);
            painter.drawImage(0, 0, viewerImage);
            painter.drawImage(viewerImage.width(), 0, viewerImage.mirrored(true, false));
            break;
        }

        case LayTriple: {
            mirrorImage = QImage(viewerImage.width() * 3, viewerImage.height(),
                                 QImage::Format_ARGB32);
            QPainter painter(&mirrorImage);
            painter.drawImage(0, 0, viewerImage);
            painter.drawImage(viewerImage.width(), 0, viewerImage.mirrored(true, false));
            painter.drawImage(viewerImage.width() * 2, 0, viewerImage.mirrored(false, false));
            break;
        }

        case LayQuad: {
            mirrorImage = QImage(viewerImage.width() * 2, viewerImage.height() * 2,
                                 QImage::Format_ARGB32);
            QPainter painter(&mirrorImage);
            painter.drawImage(0, 0, viewerImage);
            painter.drawImage(viewerImage.width(), 0, viewerImage.mirrored(true, false));
            painter.drawImage(0, viewerImage.height(), viewerImage.mirrored(false, true));
            painter.drawImage(viewerImage.width(), viewerImage.height(),
                              viewerImage.mirrored(true, true));
            break;
        }

        case LayVDual: {
            mirrorImage = QImage(viewerImage.width(), viewerImage.height() * 2,
                                 QImage::Format_ARGB32);
            QPainter painter(&mirrorImage);
            painter.drawImage(0, 0, viewerImage);
            painter.drawImage(0, viewerImage.height(), viewerImage.mirrored(false, true));
            break;
        }
    }

    viewerImage = mirrorImage;
}

template<typename T>
static inline T bound0To255(const T val) {
    return ((val > 255) ? 255 : (val < 0) ? 0 : val);
}


#if defined(Q_CC_GNU) && __has_cpp_attribute(optimize)
#pragma GCC push_options
#pragma GCC optimize ("-O3")
#endif
void ImageViewer::colorize() {
    QElapsedTimer t; t.start();

    // I'm too lazy to implement more compatibility
    switch(viewerImage.format()) {
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32:
        break;
    default:
        viewerImage = viewerImage.convertToFormat(QImage::Format_RGB32);

    }

    // Create LUT for brightness/contrast adjustment
    const float contrast = Settings::contrastVal / 100.0f;
    const float brightness = Settings::brightVal / 100.0f;
    static unsigned char brightContrastTransform[256];
    for (int i = 0; i < 256; ++i) {
        float contrastTransform;
        if (i < (int) (128.0f + 128.0f * tan(contrast)) && i > (int) (128.0f - 128.0f * tan(contrast))) {
            contrastTransform = (i - 128.) / tan(contrast) + 128.;
        } else if (i >= (int) (128.0f + 128.0f * tan(contrast))) {
            contrastTransform = 255;
        } else {
            contrastTransform = 0;
        }

        brightContrastTransform[i] = qMin(255, int((255.0 * pow(contrastTransform / 255.0, 1.0 / brightness)) + 0.5));
    }

    const int width = viewerImage.width();
    std::function<void(uchar*)> lineFunc = [width](uchar *line) {
        static thread_local std::vector<uchar> sourceRow;
        sourceRow.resize(width * 3, 255);
        uchar *source = sourceRow.data();

        // First do basic RGB modifications
        const uchar redVal = Settings::redVal + 128;
        const uchar greenVal = Settings::greenVal + 128;
        const uchar blueVal = Settings::blueVal + 128;
        const bool rNegateEnabled = Settings::rNegateEnabled, gNegateEnabled = Settings::gNegateEnabled, bNegateEnabled = Settings::bNegateEnabled;
        for (int x = 0; x < width; ++x) {
            unsigned r = rNegateEnabled ? 255 - line[x * 4 + 2] : line[x * 4 + 2];
            unsigned g = gNegateEnabled ? 255 - line[x * 4 + 1] : line[x * 4 + 1];
            unsigned b = bNegateEnabled ? 255 - line[x * 4 + 0] : line[x * 4 + 0];

            r = bound0To255(r * redVal / 128);
            g = bound0To255(g * greenVal / 128);
            b = bound0To255(b * blueVal / 128);

            r = brightContrastTransform[r];
            g = brightContrastTransform[g];
            b = brightContrastTransform[b];

            source[x * 3 + 0] = r;
            source[x * 3 + 1] = g;
            source[x * 3 + 2] = b;
        }

        static thread_local std::vector<float> rgbRow;
        rgbRow.resize(width * 4, 1.);
        float *rgb = rgbRow.data();
        for (int x = 0; x < width; x++) {
            rgb[x * 4 + 1] = source[x * 3 + 0];
            rgb[x * 4 + 2] = source[x * 3 + 1];
            rgb[x * 4 + 3] = source[x * 3 + 2];
        }

        // Convert to hsv
        static thread_local std::vector<float> hsvRow;
        hsvRow.resize(width * 4, 1.);
        float *hsv = hsvRow.data();
        ahsv_from_argb(hsv, rgb, width);

        // Do HSV modifications
        const float hueVal = Settings::hueVal / 255.f;
        const float saturationVal = Settings::saturationVal / 100.f;
        const float lightnessVal = Settings::lightnessVal / 100.f;
        for (int x = 0; x < width; ++x) {
            if (Settings::colorizeEnabled) {
                hsv[x * 4 + 1] = hueVal;
            } else {
                hsv[x * 4 + 1] += hueVal;
                if (hsv[x * 4 + 1] < 0.f) {
                    hsv[x * 4 + 1] += 1.f;
                } else if (hsv[x * 4 + 1] > 1.f) {
                    hsv[x * 4 + 1] -= 1.f;
                }
            }

            hsv[x * 4 + 2] *= saturationVal;
            hsv[x * 4 + 3] *= lightnessVal;
        }

        // Convert back to rgb from hsv
        argb_from_ahsv(rgb, hsv, width);

        // Replace original image data
        const bool hueRedChannel = Settings::hueRedChannel, hueGreenChannel = Settings::hueGreenChannel, hueBlueChannel = Settings::hueBlueChannel;
        for (int x = 0; x < width; ++x) {
            if (hueRedChannel) { // keeping this in the loop doesn't matter, it gets perfects hits on the branch predictor
                line[x * 4 + 2] = rgb[x * 4 + 1];
            }
            if (hueGreenChannel) {
                line[x * 4 + 1] = rgb[x * 4 + 2];
            }
            if (hueBlueChannel) {
                line[x * 4 + 0] = rgb[x * 4 + 3];
            }
        }
    };
    QSemaphore semaphore;
    int segments = (qsizetype(viewerImage.height()) * viewerImage.width()) / (1<<16);
    segments = std::min(segments, viewerImage.height());
    if (segments > 1) {
        uchar *bits = viewerImage.bits();
        size_t bpl = viewerImage.bytesPerLine();
        const int height = viewerImage.height();

        for (int i=0, y=0; i<segments; i++) {
            int num = (height - y) / (segments - i);
            QThreadPool::globalInstance()->start([&semaphore, bits, bpl, &lineFunc, num]() {
                uchar *line = bits;
                for (int y=0; y<num; y++) {
                    lineFunc(line);
                    line += bpl;
                }
                semaphore.release(num);
            });
            bits += bpl * num;
            y += num;
        }
    } else {
        for (int y = 0; y < viewerImage.height(); ++y) {
            uchar *line = viewerImage.scanLine(y);
            QThreadPool::globalInstance()->start([&semaphore, line, &lineFunc]() {
                lineFunc(line);
                semaphore.release(1);
            });
        }
    }
    // Wait for all threads to complete
    semaphore.acquire(viewerImage.height());

    static const bool benchmark = qEnvironmentVariableIsSet("PHOTOTONIC_BENCHMARK");
    if (benchmark) {
        qDebug() << "Color effect applied in" << t.elapsed();
    }
}
#if defined(Q_CC_GNU) && __has_cpp_attribute(optimize)
#pragma GCC pop_options
#endif

void ImageViewer::refresh() {
    if (!imageWidget) {
        return;
    }

    if (Settings::scaledWidth) {
        viewerImage = origImage.scaled(Settings::scaledWidth, Settings::scaledHeight,
                                       Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    } else {
        viewerImage = origImage;
    }

    transform();

    if (Settings::colorsActive || Settings::keepTransform) {
        colorize();
    }

    if (mirrorLayout) {
        mirror();
    }

    imageWidget->setImage(viewerImage, true);
    resizeImage();
}

void ImageViewer::setImage(const QImage &image) {
    //if (movieWidget) {
    //    delete movieWidget;
    //    movieWidget = nullptr;
    //    imageWidget = new ImageWidget;
    //    //scrollArea->setWidget(imageWidget);
    //}

    imageWidget->setImage(image, false);
    if (!Settings::keepTransform) {
        cropRubberBand->hide();
        resizeImage();
    }
}

QImage createImageWithOverlay(const QImage &baseImage, const QImage &overlayImage, int x, int y) {
    QImage imageWithOverlay = QImage(overlayImage.size(), QImage::Format_ARGB32_Premultiplied);
    QPainter painter(&imageWithOverlay);

    QImage scaledImage = baseImage.scaled(overlayImage.width(), overlayImage.height(),
                                          Qt::KeepAspectRatio, Qt::SmoothTransformation);

    painter.setCompositionMode(QPainter::CompositionMode_Source);
    painter.fillRect(imageWithOverlay.rect(), Qt::transparent);

    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.drawImage(x, y, scaledImage);

    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.drawImage(0, 0, overlayImage);

    painter.end();

    return imageWithOverlay;
}

void ImageViewer::reload() {
    if (Settings::showImageName) {
        if (viewerImageFullPath.left(1) == ":") {
            setInfo("No Image");
        } else if (viewerImageFullPath.isEmpty()) {
            setInfo("Clipboard");
        } else {
            setInfo(QFileInfo(viewerImageFullPath).fileName());
        }
    }

    if (!Settings::keepTransform) {
        Settings::cropLeftPercent = Settings::cropTopPercent = Settings::cropWidthPercent = Settings::cropHeightPercent = 0;
        Settings::rotation = 0;
        Settings::flipH = Settings::flipV = false;
    }
    Settings::scaledWidth = Settings::scaledHeight = 0;
    if (!batchMode) {
        Settings::mouseRotateEnabled = false;
        emit toolsUpdated();

        if (!Settings::keepTransform)
            Settings::cropLeft = Settings::cropTop = Settings::cropWidth = Settings::cropHeight = 0;
        if (newImage || viewerImageFullPath.isEmpty()) {

            newImage = true;
            viewerImageFullPath = CLIPBOARD_IMAGE_NAME;
            origImage.load(":/images/no_image.png");
            viewerImage = origImage;
            setImage(viewerImage);
            pasteImage();
            return;
        }
    }

    QImageReader imageReader(viewerImageFullPath);
    if (batchMode && imageReader.supportsAnimation()) {
        qWarning() << tr("skipping animation in batch mode:") << viewerImageFullPath;
        return;
    }
    if (Settings::enableAnimations && imageReader.supportsAnimation()) {
        if (imageWidget->loadAnimation(viewerImageFullPath)) {
            resizeImage();
            return;
        }
    }

    // It's not a movie

    imageReader.read(&origImage);
    if (origImage.size().isValid()) {
        if (origImage.colorSpace() != QColorSpace::SRgb) {
            origImage.convertToColorSpace(QColorSpace::SRgb);
        }
        if (Settings::exifRotationEnabled) {
            rotateByExifRotation(origImage, viewerImageFullPath);
        }
        viewerImage = origImage;

        if (Settings::colorsActive || Settings::keepTransform) {
            colorize();
        }
        if (mirrorLayout) {
            mirror();
        }
    } else {
        viewerImage = QIcon::fromTheme("image-missing",
                                        QIcon(":/images/error_image.png")).pixmap(BAD_IMAGE_SIZE, BAD_IMAGE_SIZE).toImage();
        setInfo(QFileInfo(imageReader.fileName()).fileName() + ": " + imageReader.errorString());
    }

    setImage(viewerImage);
    resizeImage();
    if (Settings::keepTransform) {
        if (Settings::cropLeft || Settings::cropTop || Settings::cropWidth || Settings::cropHeight) {
            if (cropRubberBand) {
                cropRubberBand->show();
                updateRubberBandFeedback();
            }
        }
        imageWidget->setRotation(Settings::rotation);
    }
    if (Settings::setWindowIcon) {
        QPixmap icon;
        icon.convertFromImage(viewerImage.scaled(WINDOW_ICON_SIZE, WINDOW_ICON_SIZE,
                                                 Qt::KeepAspectRatio, Qt::SmoothTransformation));
        phototonic->setWindowIcon(icon);
    }
}

void ImageViewer::setInfo(QString infoString) {
    imageInfoLabel->setText(infoString);
    imageInfoLabel->adjustSize();
}

void ImageViewer::unsetFeedback() {
    feedbackLabel->clear();
    feedbackLabel->setVisible(false);
}

void ImageViewer::setFeedback(QString feedbackString, bool timeLimited) {
    if (feedbackString.isEmpty())
        return;
    feedbackLabel->setText(feedbackString);
    feedbackLabel->setVisible(true);

    int margin = imageInfoLabel->isVisible() ? (imageInfoLabel->height() + 15) : 10;
    feedbackLabel->move(10, margin);

    feedbackLabel->adjustSize();
    if (timeLimited)
        QTimer::singleShot(3000, this, SLOT(unsetFeedback()));
}

void ImageViewer::loadImage(QString imageFileName) {
    newImage = false;
    tempDisableResize = false;
    viewerImageFullPath = imageFileName;

    if (!Settings::keepZoomFactor) {
        Settings::imageZoomFactor = 1.0;
    }

    QApplication::processEvents();
    reload();
}

void ImageViewer::clearImage() {
    origImage.load(":/images/no_image.png");
    viewerImage = origImage;
    setImage(viewerImage);
}

void ImageViewer::monitorCursorState() {
    static QPoint lastPos;

    if (QCursor::pos() != lastPos) {
        lastPos = QCursor::pos();
        if (cursorIsHidden) {
            QApplication::restoreOverrideCursor();
            cursorIsHidden = false;
        }
    } else {
        if (!cursorIsHidden) {
            QApplication::setOverrideCursor(Qt::BlankCursor);
            cursorIsHidden = true;
        }
    }
}

void ImageViewer::setCursorHiding(bool hide) {
    if (hide) {
        mouseMovementTimer->start(500);
    } else {
        mouseMovementTimer->stop();
        if (cursorIsHidden) {
            QApplication::restoreOverrideCursor();
            cursorIsHidden = false;
        }
    }
}

void ImageViewer::mouseDoubleClickEvent(QMouseEvent *event) {
    QWidget::mouseDoubleClickEvent(event);
    while (QApplication::overrideCursor()) {
        QApplication::restoreOverrideCursor();
    }
}

void ImageViewer::mousePressEvent(QMouseEvent *event) {
    if (!imageWidget) {
        return;
    }
    if (event->button() == Qt::LeftButton) {
        if (event->modifiers() == Qt::ControlModifier) {
            cropOrigin = event->pos();
            cropRubberBand->show();
            cropRubberBand->setGeometry(QRect(cropOrigin, event->pos()).normalized());
        } else if (cropRubberBand) {
            QRect cropRect = cropRubberBand->geometry() + QMargins(10, 10, 10, 10);
            if (!cropRect.contains(event->pos())) {
                cropRubberBand->hide();
                imageWidget->setCropRect(QRect());
            }
        }
        initialRotation = imageWidget->rotation();
        setMouseMoveData(true, event->x(), event->y());
        QApplication::setOverrideCursor(Qt::ClosedHandCursor);
        event->accept();
    }
    QWidget::mousePressEvent(event);
}

void ImageViewer::mouseReleaseEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton) {
        setMouseMoveData(false, 0, 0);
        while (QApplication::overrideCursor()) {
            QApplication::restoreOverrideCursor();
        }
    }

    QWidget::mouseReleaseEvent(event);
}

void ImageViewer::updateRubberBandFeedback() {
    if (!imageWidget) {
        return;
    }
    if (!cropRubberBand || !cropRubberBand->isVisible()) {
        imageWidget->setCropRect(QRect());
        imageWidget->update();
        unsetFeedback();
        return;
    }

    QRect geom = cropRubberBand->geometry().normalized();
    QPoint cropTopLeft = imageWidget->mapFromGlobal(mapToGlobal(geom.topLeft()));
    imageWidget->setCropRect(QRect(cropTopLeft, cropRubberBand->size()));

    QPoint bandTopLeft = imageWidget->mapToImage(cropTopLeft);

    setFeedback(tr("Selection: ")
                + QString::number(geom.width())
                + "x"
                + QString::number(geom.height())
                + (bandTopLeft.x() < 0 ? "" : "+")
                + QString::number(bandTopLeft.x())
                + (bandTopLeft.y() < 0 ? "" : "+")
                + QString::number(bandTopLeft.y()), false);
}

void ImageViewer::applyCropAndRotation() {
    if (!imageWidget) {
        return;
    }

    bool didSomething = false;
    if (cropRubberBand && cropRubberBand->isVisible()) {

        QPoint bandTopLeft = mapToGlobal(cropRubberBand->geometry().topLeft());
        QPoint bandBottomRight = mapToGlobal(cropRubberBand->geometry().bottomRight());

        bandTopLeft = imageWidget->mapToImage(imageWidget->mapFromGlobal(bandTopLeft));
        bandBottomRight = imageWidget->mapToImage(imageWidget->mapFromGlobal(bandBottomRight));
        double scaledX = imageWidget->imageSize().width();
        double scaledY = imageWidget->imageSize().height();
        scaledX = viewerImage.width() / scaledX;
        scaledY = viewerImage.height() / scaledY;

        bandTopLeft.setX(int(bandTopLeft.x() * scaledX));
        bandTopLeft.setY(int(bandTopLeft.y() * scaledY));
        bandBottomRight.setX(int(bandBottomRight.x() * scaledX));
        bandBottomRight.setY(int(bandBottomRight.y() * scaledY));

        Settings::cropLeft = bandTopLeft.x();
        Settings::cropTop = bandTopLeft.y();
        Settings::cropWidth = viewerImage.width() - bandBottomRight.x();
        Settings::cropHeight = viewerImage.height() - bandBottomRight.y();
        Settings::rotation = imageWidget->rotation();

        if (!qFuzzyCompare(Settings::imageZoomFactor, 1.f)) {
            qreal deltaX = qreal(Settings::cropWidth) / imageWidget->imageSize().width();
            qreal deltaY = qreal(Settings::cropHeight) / imageWidget->imageSize().height();
            Settings::imageZoomFactor = qBound(0.25, Settings::imageZoomFactor * qMin(deltaX, deltaY), 16.);
        }

        cropRubberBand->hide();
        updateRubberBandFeedback();
        refresh();
        didSomething = true;
    }
    if (!qFuzzyCompare(imageWidget->rotation(), 0)) {
        refresh();
        imageWidget->setRotation(0);
        didSomething = true;
    }
    if (!didSomething) {
        MessageBox messageBox(this);
        messageBox.warning(tr("No selection for cropping, and no rotation"),
                           tr("To make a selection, hold down the Ctrl key and select a region using the mouse. "
                              "To rotate, hold down the Ctrl and Shift keys and drag the mouse near the right edge."));
    }
}

void ImageViewer::setMouseMoveData(bool lockMove, int lMouseX, int lMouseY) {
    if (!imageWidget) {
        return;
    }
    moveImageLocked = lockMove;
    mouseX = lMouseX;
    mouseY = lMouseY;
    layoutX = imageWidget->viewRect().x(); //imageWidget->pos().x();
    layoutY = imageWidget->viewRect().y(); //imageWidget->pos().y();
}

void ImageViewer::mouseMoveEvent(QMouseEvent *event) {
    if (!imageWidget) {
        return;
    }

    if (Settings::mouseRotateEnabled) {
        QPointF fulcrum(QPointF(imageWidget->pos()) + QPointF(imageWidget->width() / 2.0, imageWidget->height() / 2.0));
        if (event->pos().x() > (width() * 3) / 4)
            fulcrum.setY(mouseY); // if the user pressed near the right edge, start with initial rotation of 0
        QLineF vector(fulcrum, event->localPos());
        imageWidget->setRotation(initialRotation - vector.angle());
        // qDebug() << "image center" << fulcrum << "line" << vector << "angle" << vector.angle() << "geom" << imageWidget->geometry();

    } else if (event->modifiers() & Qt::ControlModifier) {
        if (!cropRubberBand || !cropRubberBand->isVisible()) {
            return;
        }
        QPoint eventPos = cropRubberBand->boundedPoint(event->pos());
        int deltaX = cropOrigin.x() - eventPos.x();
        if (qAbs(deltaX) < cropRubberBand->minimumWidth()) {
            deltaX = cropRubberBand->minimumWidth() * (deltaX < 0 ? -1 : 1);
        }
        int deltaY = cropOrigin.y() - eventPos.y();
        if (qAbs(deltaY) < cropRubberBand->minimumHeight()) {
            deltaY = cropRubberBand->minimumWidth() * (deltaY < 0 ? -1 : 1);
        }

        QSize size;

        // Force square
        if (event->modifiers() & Qt::ShiftModifier || Settings::squareCrop) {
            size = QSize(-deltaX, deltaY < 0 ? qAbs(deltaX) : -qAbs(deltaX));
        } else {
            size = QSize(-deltaX, -deltaY);
        }

        cropRubberBand->setGeometry(QRect(cropOrigin, size).normalized());
    } else {
        if (moveImageLocked) {
            int newX = layoutX + (event->pos().x() - mouseX);
            int newY = layoutY + (event->pos().y() - mouseY);
            bool needToMove = true;

            if (imageWidget->viewRect().width() > size().width()) {
                if (newX > 0) {
                    newX = 0;
                } else if (newX < (size().width() - imageWidget->viewRect().width())) {
                    newX = (size().width() - imageWidget->viewRect().width());
                }
                needToMove = true;
            } else {
                newX = layoutX;
            }

            if (imageWidget->viewRect().height() > size().height()) {
                if (newY > 0) {
                    newY = 0;
                } else if (newY < (size().height() - imageWidget->viewRect().height())) {
                    newY = (size().height() - imageWidget->viewRect().height());
                }
                needToMove = true;
            } else {
                newY = layoutY;
            }

            if (needToMove) {
                QRectF imageRect = imageWidget->viewRect();
                imageRect.moveTo(newX, newY);
                imageWidget->setViewRect(imageRect);
            }
        }
    }
}

void ImageViewer::keyMoveEvent(int direction) {
    if (!imageWidget) {
        return;
    }

    int newX = layoutX = imageWidget->viewRect().x();
    int newY = layoutY = imageWidget->viewRect().y();
    bool needToMove = false;

    switch (direction) {
        case MoveLeft:
            newX += 50;
            break;
        case MoveRight:
            newX -= 50;
            break;
        case MoveUp:
            newY += 50;
            break;
        case MoveDown:
            newY -= 50;
            break;
    }

    if (imageWidget->viewRect().width() > size().width()) {
        if (newX > 0) {
            newX = 0;
        } else if (newX < (size().width() - imageWidget->viewRect().width())) {
            newX = (size().width() - imageWidget->viewRect().width());
        }
        needToMove = true;
    } else {
        newX = layoutX;
    }

    if (imageWidget->viewRect().height() > size().height()) {
        if (newY > 0) {
            newY = 0;
        } else if (newY < (size().height() - imageWidget->viewRect().height())) {
            newY = (size().height() - imageWidget->viewRect().height());
        }
        needToMove = true;
    } else {
        newY = layoutY;
    }

    if (needToMove) {
        QRectF imageRect = imageWidget->viewRect();
        imageRect.moveTo(newX, newY);
        imageWidget->setViewRect(imageRect);
    }
}

void ImageViewer::saveImage() {
    IGNORE_EXIV2_DEPRECATED_START;
    Exiv2::Image::AutoPtr image;
    IGNORE_EXIV2_DEPRECATED_END;

    bool exifError = false;
    static bool showExifError = true;

    if (newImage) {
        saveImageAs();
        return;
    }

    setFeedback(tr("Saving..."));

    try {
        image = Exiv2::ImageFactory::open(viewerImageFullPath.toStdString());
        image->readMetadata();
    }
    catch (const Exiv2::Error &error) {
        qWarning() << "EXIV2:" << error.what();
        exifError = true;
    }

    QImageReader imageReader(viewerImageFullPath);
    QString savePath = viewerImageFullPath;
    if (!Settings::saveDirectory.isEmpty()) {
        QDir saveDir(Settings::saveDirectory);
        savePath = saveDir.filePath(QFileInfo(viewerImageFullPath).fileName());
    }
    if (!viewerImage.save(savePath, imageReader.format().toUpper(), Settings::defaultSaveQuality)) {
        MessageBox msgBox(this);
        msgBox.critical(tr("Error"), tr("Failed to save image."));
        return;
    }

    if (!exifError) {
        try {
            if (Settings::saveDirectory.isEmpty()) {
                image->writeMetadata();
            } else {
                IGNORE_EXIV2_DEPRECATED_START;
                Exiv2::Image::AutoPtr imageOut = Exiv2::ImageFactory::open(savePath.toStdString());
                IGNORE_EXIV2_DEPRECATED_END;

                imageOut->setMetadata(*image);
                Exiv2::ExifThumb thumb(imageOut->exifData());
                thumb.erase();
                // TODO: thumb.setJpegThumbnail(thumbnailPath);
                imageOut->writeMetadata();
            }
        }
        catch (Exiv2::Error &error) {
            if (showExifError) {
                MessageBox msgBox(this);
                QCheckBox cb(tr("Don't show this message again"));
                msgBox.setCheckBox(&cb);
                msgBox.critical(tr("Error"), tr("Failed to save Exif metadata."));
                showExifError = !(cb.isChecked());
            } else {
                qWarning() << tr("Failed to safe Exif metadata:") << error.what();
            }
        }
    }

    reload();
    setFeedback(tr("Image saved."));
}

void ImageViewer::saveImageAs() {
    IGNORE_EXIV2_DEPRECATED_START;
    Exiv2::Image::AutoPtr exifImage;
    Exiv2::Image::AutoPtr newExifImage;
    IGNORE_EXIV2_DEPRECATED_END;

    bool exifError = false;

    setCursorHiding(false);

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save image as"),
                                                    viewerImageFullPath,
                                                    tr("Images") +
                                                    " (*.jpg *.jpeg *.png *.bmp *.tif *.tiff *.ppm *.pgm *.pbm *.xbm *.xpm *.cur *.ico *.icns *.wbmp *.webp)");

    if (!fileName.isEmpty()) {
        try {
            exifImage = Exiv2::ImageFactory::open(viewerImageFullPath.toStdString());
            exifImage->readMetadata();
        }
        catch (const Exiv2::Error &error) {
            qWarning() << "EXIV2" << error.what();
            exifError = true;
        }


        if (!viewerImage.save(fileName, 0, Settings::defaultSaveQuality)) {
            MessageBox msgBox(this);
            msgBox.critical(tr("Error"), tr("Failed to save image."));
        } else {
            if (!exifError) {
                try {
                    newExifImage = Exiv2::ImageFactory::open(fileName.toStdString());
                    newExifImage->setMetadata(*exifImage);
                    newExifImage->writeMetadata();
                }
                catch (Exiv2::Error &error) {
                    exifError = true;
                }
            }

            setFeedback(tr("Image saved."));
        }
    }
    if (phototonic->isFullScreen()) {
        setCursorHiding(true);
    }
}

void ImageViewer::contextMenuEvent(QContextMenuEvent *) {
    while (QApplication::overrideCursor()) {
        QApplication::restoreOverrideCursor();
    }
    contextMenuPosition = QCursor::pos();
    ImagePopUpMenu->exec(contextMenuPosition);
}

int ImageViewer::getImageWidthPreCropped() {
    return origImage.width();
}

int ImageViewer::getImageHeightPreCropped() {
    return origImage.height();
}

bool ImageViewer::isNewImage() {
    return newImage;
}

void ImageViewer::copyImage() {
    QApplication::clipboard()->setImage(viewerImage);
}

void ImageViewer::pasteImage() {
    if (!imageWidget) {
        return;
    }

    if (!QApplication::clipboard()->image().isNull()) {
        origImage = QApplication::clipboard()->image();
        refresh();
    }
    phototonic->setWindowTitle(tr("Clipboard") + " - Phototonic");
    if (Settings::setWindowIcon) {
        phototonic->setWindowIcon(phototonic->getDefaultWindowIcon());
    }
}

void ImageViewer::setBackgroundColor() {
    QString bgColor = "background: rgb(%1, %2, %3); ";
    bgColor = bgColor.arg(Settings::viewerBackgroundColor.red())
            .arg(Settings::viewerBackgroundColor.green())
            .arg(Settings::viewerBackgroundColor.blue());

    QString styleSheet = "QWidget { " + bgColor + " }";
    imageWidget->setStyleSheet(styleSheet);
    //scrollArea->setStyleSheet(styleSheet);
}

QPoint ImageViewer::getContextMenuPosition() {
    return contextMenuPosition;
}

