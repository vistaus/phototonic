/*
 *  Copyright (C) 2013-2014 Ofer Kashayov <oferkv@live.com>
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ImagePreview.h"
#include "Settings.h"
#include "ThumbsViewer.h"
#include "ImageViewer.h"

#include <QMovie>

ImagePreview::ImagePreview(QWidget *parent) : QWidget(parent)
{
    setFocusProxy(parent);

    imageLabel = new QLabel;
    imageLabel->setScaledContents(true);

    scrollArea = new QScrollArea;
    scrollArea->setContentsMargins(0, 0, 0, 0);
    scrollArea->setAlignment(Qt::AlignCenter);
    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea->verticalScrollBar()->blockSignals(true);
    scrollArea->horizontalScrollBar()->blockSignals(true);
    scrollArea->setFrameStyle(0);
    scrollArea->setWidget(imageLabel);
    scrollArea->setWidgetResizable(true);

    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(scrollArea);
    setBackgroundColor();

    imageInfoLabel = new QLabel(scrollArea);
    imageInfoLabel->setVisible(Settings::showImageName);
    imageInfoLabel->setMargin(3);
    imageInfoLabel->setVisible(true);
    imageInfoLabel->move(10, 10);
    imageInfoLabel->setStyleSheet("QLabel { background-color : black; color : white; border-radius: 3px} ");
    imageInfoLabel->setVisible(Settings::showImageName);

    QGraphicsOpacityEffect *infoEffect = new QGraphicsOpacityEffect;
    infoEffect->setOpacity(0.5);
    imageInfoLabel->setGraphicsEffect(infoEffect);

    animationEnableAction = new QAction(tr("Enable animation"), this);
    animationEnableAction->setCheckable(true);
    animationEnableAction->setChecked(Settings::enableAnimations);
    connect(animationEnableAction, &QAction::toggled, this, &ImagePreview::onEnableAnimationChanged);
    addAction(animationEnableAction);

    upscaleChangeAction = new QAction(tr("Upscale small images"), this);
    upscaleChangeAction->setCheckable(true);
    upscaleChangeAction->setChecked(Settings::enableAnimations);
    connect(upscaleChangeAction, &QAction::toggled, this, &ImagePreview::onUpscaleChanged);
    addAction(upscaleChangeAction);

    showFileNameAction = new QAction(tr("Show filename"), this);
    showFileNameAction->setCheckable(true);
    showFileNameAction->setChecked(Settings::showImageName);
    connect(showFileNameAction, &QAction::toggled, this, &ImagePreview::onShowFileName);
    addAction(showFileNameAction);

    setContextMenuPolicy(Qt::ActionsContextMenu);

    setLayout(mainLayout);
}

QPixmap& ImagePreview::loadImage(QString imageFileName) {
    upscaleChangeAction->setChecked(Settings::upscalePreview);
    animationEnableAction->setChecked(Settings::enableAnimations);

    imageInfoLabel->setText(QFileInfo(imageFileName).fileName());
    imageInfoLabel->setVisible(Settings::showImageName);
    imageInfoLabel->adjustSize();

    currentFileName = imageFileName;

    if (animation) {
        delete animation;
    }

    QImageReader imageReader(imageFileName);

    if (Settings::enableAnimations && imageReader.supportsAnimation()){
        animation = new QMovie(imageFileName);
        animation->setParent(imageLabel);
        animation->start();
        imageLabel->setMovie(animation);
        previewPixmap = animation->currentPixmap();
    } else {
        QImage previewImage;
        imageReader.read(&previewImage);
        if (!previewImage.isNull()) {
            if (previewImage.colorSpace() != QColorSpace::SRgb) {
                previewImage.convertToColorSpace(QColorSpace::SRgb);
            }

            if (Settings::exifRotationEnabled) {
                imageViewer->rotateByExifRotation(previewImage, imageFileName);
            }
            previewPixmap = QPixmap::fromImage(previewImage);
        } else {
            previewPixmap = QIcon::fromTheme("image-missing",
                                             QIcon(":/images/error_image.png")).pixmap(BAD_IMAGE_SIZE, BAD_IMAGE_SIZE);
        }
    }
    if (animation) {
        imageLabel->setMovie(animation);
    } else {
        imageLabel->setPixmap(previewPixmap);
    }

    resizeImagePreview();
    return previewPixmap;
}

void ImagePreview::onShowFileName(bool enabled) {
    Settings::showImageName = enabled;
    imageInfoLabel->setVisible(Settings::showImageName);
}

void ImagePreview::onUpscaleChanged(bool enabled) {
    Settings::upscalePreview = enabled;
    if (!currentFileName.isEmpty()) {
        loadImage(currentFileName);
    }
}

void ImagePreview::onEnableAnimationChanged(bool enabled) {
    Settings::enableAnimations = enabled;
    if (!currentFileName.isEmpty()) {
        loadImage(currentFileName);
    }
}

void ImagePreview::clear() {
    imageLabel->clear();
}

void ImagePreview::resizeImagePreview()
{
    QSize previewSizePixmap = previewPixmap.size();
    if (Settings::upscalePreview || previewSizePixmap.width() > scrollArea->width() || previewSizePixmap.height() > scrollArea->height()) {
        previewSizePixmap.scale(scrollArea->width(), scrollArea->height(), Qt::KeepAspectRatio);
    }
    if (!animation && !previewPixmap.isNull() && !size().isEmpty()) {
        bool smoothScale = width() / previewPixmap.width() < 10 || height() / previewPixmap.height() < 10;
        QPixmap scaled = previewPixmap;

        if (Settings::upscalePreview || previewSizePixmap.width() > scrollArea->width() || previewSizePixmap.height() > scrollArea->height()) {
            scaled = scaled.scaled(previewSizePixmap, Qt::KeepAspectRatio, smoothScale ? Qt::SmoothTransformation : Qt::FastTransformation);
        }
        imageLabel->setPixmap(scaled);
    }

    imageLabel->setFixedSize(previewSizePixmap);
    imageLabel->adjustSize();
}

void ImagePreview::resizeEvent(QResizeEvent *event) {
    QWidget::resizeEvent(event);
    resizeImagePreview();
}

void ImagePreview::setBackgroundColor() {
    QString bgColor = "background: rgb(%1, %2, %3); ";
    bgColor = bgColor.arg(Settings::thumbsBackgroundColor.red())
            .arg(Settings::thumbsBackgroundColor.green()).arg(Settings::thumbsBackgroundColor.blue());

    QString ss = "QWidget { " + bgColor + " }";
    scrollArea->setStyleSheet(ss);
}

void ImagePreview::setImageViewer(ImageViewer *imageViewer) {
    this->imageViewer = imageViewer;
}
