#ifndef _RGBHSV_H
#define _RGBHSV_H

// Pure C++ Implementation.
void ahsv_from_argb_c(float* dst, const float* src, int length);
void argb_from_ahsv_c(float* dst, const float* src, int length);

#if __SSE2__
// SSE2 Enhanced Implementation.
void ahsv_from_argb_sse2(float* dst, const float* src, int length);
void argb_from_ahsv_sse2(float* dst, const float* src, int length);

#define ahsv_from_argb ahsv_from_argb_sse2
#define argb_from_ahsv argb_from_ahsv_sse2
#else
#define ahsv_from_argb ahsv_from_argb_c
#define argb_from_ahsv argb_from_ahsv_c
#endif

#endif // _RGBHSV_H
