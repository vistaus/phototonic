/*
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include <QTimer>
#include <QImageReader>
#include <QElapsedTimer>

struct Animation : public QObject
{
    Q_OBJECT
public:
    enum State {
        Playing,
        Paused
    };
    Q_ENUM(State);
    State state = Paused;

    Animation(QObject *parent = nullptr);
    const QImage &load(QIODevice *device);

    int speed = 100;

    bool cacheFrames = true;

    QVector<QImage> imageCache;
    QVector<int> m_delayCache;

    operator bool() { return reader.device() != nullptr; }

    QSize currentImageSize() const { return currentImage.size(); }

    int frameCount();

    void jumpToImage(const int num);
    int currentImageNumber() const { return m_currentFrame; }

    QByteArray format() const { return reader.format(); }

public slots:
    void start();
    void pause();

signals:
    void nextFrameReady(QImage image);
    void error(const QString &message);

private slots:
    void onTimeout() {
        if (!readNextImage(true)) {
            handleReaderError(true);
        }
    }

private:
    void setNextTimer(int delay, int processingTime);
    bool readNextImage(bool startTimer);
    void handleReaderError(bool startTimer);

    QTimer m_timer;
    int m_currentFrame = 0;
    int m_largestImageNumber = 0;

    int m_timeLeft = 0;
    QImageReader reader;

    bool m_cacheFull = false;

    bool m_brokenFile = false;

    QImage currentImage;
};

