/*
 *  Copyright (C) 2013-2014 Ofer Kashayov <oferkv@live.com>
 *  This file is part of Phototonic Image Viewer.
 *
 *  Phototonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phototonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Phototonic.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMimeDatabase>
#include <QProgressDialog>
#include <QRandomGenerator>

#include "ThumbsViewer.h"
#include "Exiv.h"
#include "Phototonic.h"
#include "SmartCrop.h"
#include "ImagePreview.h"
#include "InfoViewer.h"
#include "Tags.h"
#include "MetadataCache.h"
#include "ImageViewer.h"

#include <bitset>

#ifdef __GNUC__
#define POPCNT(x) __builtin_popcountll(x)
#else
static inline uint64_t our_popcount64(uint64_t x)
{
  uint64_t m1 = 0x5555555555555555ll;
  uint64_t m2 = 0x3333333333333333ll;
  uint64_t m4 = 0x0F0F0F0F0F0F0F0Fll;
  uint64_t h01 = 0x0101010101010101ll;

  x -= (x >> 1) & m1;
  x = (x & m2) + ((x >> 2) & m2);
  x = (x + (x >> 4)) & m4;

  return (x * h01) >> 56;
}
#define POPCNT(x) our_popcount64(x)
#endif

#define BATCH_SIZE 10

// Crushes color depth to avoid minor color differences from e. g. colors
#define DUPLICATE_COLOR_QUANT 64
//#define DEBUG_DUPES

#define ANALYZE_SIZE 255

static uint64_t calcImageHash(const QImage &inputImage, const QString &filePath)
{
    QElapsedTimer t; t.start();
    QString filename = QFileInfo(filePath).baseName();

    if (inputImage.isNull()) {
        return 0;
    }
    QImage image = inputImage;

#ifdef DEBUG_DUPES
    inputImage.save("/tmp/input-" + filePath + ".png");
#endif

    if (inputImage.width() != ANALYZE_SIZE || inputImage.height() != ANALYZE_SIZE) {
        image = inputImage.scaled(ANALYZE_SIZE, ANALYZE_SIZE, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    }

#ifdef DEBUG_DUPES
    image.save("/tmp/normscale-" + filename + ".png");
#endif

    // fill in with white
    if (image.hasAlphaChannel()) {
        QSize size = image.size();
        if (size.isEmpty()) {
            qWarning() << "Got non-null image with invalid size!";
            return 0;
        }

        QImage src = image;

        image = QImage(image.size(), QImage::Format_RGB32);

        // Filling with 50% gray, or average color from input, seems to not work
        image.fill(Qt::white);

        QPainter p(&image);
        p.drawImage(image.rect(), src);
#ifdef DEBUG_DUPES
        image.save("/tmp/postalpha-" + filename + ".png");
#endif
    }

    image = image.scaled(9, 9, Qt::IgnoreAspectRatio);

#ifdef DEBUG_DUPES
    image.save("/tmp/mini-" + filename + ".png");
#endif

    image = image.convertToFormat(QImage::Format_Grayscale8);

#ifdef DEBUG_DUPES
    image.save("/tmp/gray-" + filename + ".png");
#endif

    uchar *imageBits = image.bits();
    for (int i=0; i < image.sizeInBytes(); i++) {
        // remove tiny color differences
        imageBits[i] = DUPLICATE_COLOR_QUANT * (imageBits[i] / DUPLICATE_COLOR_QUANT);
    }

#ifdef DEBUG_DUPES
    image.save("/tmp/crushed-" + filename + ".png");

    QImage diff(8, 8, QImage::Format_Grayscale8);
    diff.fill(Qt::black);
#endif
    std::bitset<64> bitset;
    for (int y=0; y<8; y++) {
        const uchar *line = image.scanLine(y);
#ifdef DEBUG_DUPES
        uchar *out = diff.scanLine(y);
#endif
        for (int x=0; x<8; x++) {
            bitset.set(y * 8 + x, line[x] > line[x+1]);
#ifdef DEBUG_DUPES
            out[x] = line[x] > line[x+1] ? 255 : 0;
#endif
        }
    }

#ifdef DEBUG_DUPES
    diff.save("/tmp/diff-" + filename + ".png");
#endif

    if (qEnvironmentVariableIsSet("PHOTOTONIC_BENCH") && t.elapsed()) {
        qDebug() << "Hashed" << filename << "in" << t.elapsed() << "ms";
    }

    return bitset.to_ullong();
}

static Histogram calcHist(const QImage &img)
{
    Histogram hist;
    if (img.isNull()) {
        return hist;
    }

    QImage image = img;

    if (image.width() != ANALYZE_SIZE || image.height() != ANALYZE_SIZE) {
        image = image.scaled(ANALYZE_SIZE, ANALYZE_SIZE, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    }

    switch(img.format()) {
    case QImage::Format_ARGB32:
    case QImage::Format_RGB32:
        break;
    case QImage::Format_ARGB32_Premultiplied:
    default:
        image = image.convertToFormat(QImage::Format_RGB32);
        break;
    }

    for (int y=0; y<ANALYZE_SIZE; y++) {
        const uchar *line = image.constScanLine(y);
        for (int x=0; x<ANALYZE_SIZE*4; x += 4) {
            hist.red  [line[x + 0]] += 1.f;
            hist.green[line[x + 1]] += 1.f;
            hist.blue [line[x + 2]] += 1.f;
        }
    }
    return hist;
}

ThumbsViewer::ThumbsViewer(QWidget *parent, const std::shared_ptr<MetadataCache> &metadataCache) : QListView(parent) {
    isBusy = false;
    this->metadataCache = metadataCache;
    Settings::thumbsBackgroundColor = Settings::appSettings->value(
            Settings::optionThumbsBackgroundColor, QColor(200, 200, 200)).value<QColor>();
    Settings::thumbsTextColor = Settings::appSettings->value(Settings::optionThumbsTextColor, QColor(25, 25, 25)).value<QColor>();
    setThumbColors();
    Settings::thumbsPagesReadCount = Settings::appSettings->value(Settings::optionThumbsPagesReadCount, 2).toUInt();
    thumbSize = Settings::appSettings->value(Settings::optionThumbsZoomLevel, 200).toInt();
    currentRow = 0;

    setViewMode(QListView::IconMode);
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    setResizeMode(QListView::Adjust);
    setWordWrap(true);
    setWrapping(true);
    setDragEnabled(true);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setUniformItemSizes(true);

    // This is the default but set for clarity. Could make it configurable to use
    // QAbstractItemView::ScrollPerPixel instead.
    setVerticalScrollMode(QAbstractItemView::ScrollPerItem);

    thumbsViewerModel = new QStandardItemModel(this);
    thumbsViewerModel->setSortRole(SortRole);
    setModel(thumbsViewerModel);

    connect(thumbsViewerModel, &QStandardItemModel::layoutChanged, this, &ThumbsViewer::onOrderingChanged);

    m_selectionChangedTimer.setInterval(10);
    m_selectionChangedTimer.setSingleShot(true);
    connect(&m_selectionChangedTimer, &QTimer::timeout, this, &ThumbsViewer::onSelectionChanged);
    connect(this->selectionModel(), &QItemSelectionModel::selectionChanged, this, [=]() {
        if (!m_selectionChangedTimer.isActive()) {
            m_selectionChangedTimer.start();
        }
    });

    m_loadThumbTimer.setInterval(10);
    m_loadThumbTimer.setSingleShot(true);
    connect(&m_loadThumbTimer, &QTimer::timeout, this, &ThumbsViewer::loadThumbsRange);
    connect(this, SIGNAL(doubleClicked(
                                 const QModelIndex &)), parent, SLOT(loadSelectedThumbImage(
                                                                             const QModelIndex &)));

    m_loadVisibleTimer.setInterval(10);
    m_loadVisibleTimer.setSingleShot(true);
    connect(&m_loadVisibleTimer, &QTimer::timeout, this, [this]() {
        loadVisibleThumbs(verticalScrollBar()->value());
    });
    connect(verticalScrollBar(), &QScrollBar::valueChanged, this, &ThumbsViewer::updateVisibleThumbnails);

    emptyImg.load(":/images/no_image.png");

    phototonic = (Phototonic *) parent;
    infoView = new InfoView(this);

    imagePreview = new ImagePreview(this);
}

void ThumbsViewer::setThumbColors() {
    QString backgroundColor = "background: rgb(%1, %2, %3); ";
    backgroundColor = backgroundColor.arg(Settings::thumbsBackgroundColor.red())
            .arg(Settings::thumbsBackgroundColor.green())
            .arg(Settings::thumbsBackgroundColor.blue());

    QString itemBackgroundColor = "background: rgba(%1, %2, %3, 0.1); ";
    itemBackgroundColor = itemBackgroundColor.arg(Settings::thumbsBackgroundColor.red())
            .arg(Settings::thumbsBackgroundColor.green())
            .arg(Settings::thumbsBackgroundColor.blue());

    QString backgroundRepeat;
    if (!Settings::thumbsRepeatBackgroundImage) {
        backgroundRepeat =
            "   background-repeat: no-repeat; "
            "   background-position: center; ";

    }


    QColor background = Settings::thumbsLayout == Squares ? Qt::transparent : Settings::thumbsBackgroundColor;
    QString styleSheet =
            "QListView { "
            "   background: " + background.name(QColor::HexRgb) + ";"
            "   background-image: url(" + Settings::thumbsBackgroundImage + ");"
            "   background-attachment: fixed; " +
            backgroundRepeat +
            "} ";
    if (Settings::thumbsLayout != Squares) {
        if (!Settings::thumbsBackgroundImage.isEmpty()) {
            QColor highlightColor = palette().highlight().color();
            highlightColor.setAlpha(128);
            styleSheet +=
                "QListView::item { "
                "   background: " + background.name(QColor::HexArgb) + ";"
                "   color: " + Settings::thumbsTextColor.name(QColor::HexRgb) + ";"
                "}"
                "QListView::item:selected { "
                "   background: " + highlightColor.name(QColor::HexArgb) + ";"
                "}"
                ;
        }
    }

    setStyleSheet(styleSheet);

    QPalette scrollBarOriginalPalette = verticalScrollBar()->palette();
    QPalette thumbViewerOriginalPalette = palette();
    thumbViewerOriginalPalette.setColor(QPalette::Text, Settings::thumbsTextColor);
    setPalette(thumbViewerOriginalPalette);
    verticalScrollBar()->setPalette(scrollBarOriginalPalette);
}

void ThumbsViewer::selectCurrentIndex() {
    if (m_currentIndex.isValid() && thumbsViewerModel->rowCount() > 0) {
        scrollTo(m_currentIndex);
        setCurrentIndex(m_currentIndex);
    }
}

QString ThumbsViewer::getSingleSelectionFilename() {
    if (selectionModel()->selectedIndexes().size() == 1)
        return thumbsViewerModel->item(selectionModel()->selectedIndexes().first().row())->data(
                FileNameRole).toString();

    return ("");
}

int ThumbsViewer::getNextRow() {
    if (currentRow == thumbsViewerModel->rowCount() - 1) {
        return -1;
    }

    return currentRow + 1;
}

int ThumbsViewer::getPrevRow() {
    if (currentRow == 0) {
        return -1;
    }

    return currentRow - 1;
}

int ThumbsViewer::getLastRow() {
    return thumbsViewerModel->rowCount() - 1;
}

int ThumbsViewer::getRandomRow() {
    return QRandomGenerator::global()->bounded(thumbsViewerModel->rowCount());
}

int ThumbsViewer::getCurrentRow() {
    return currentRow;
}

void ThumbsViewer::setCurrentRow(int row) {
    if (row >= 0) {
        currentRow = row;
    } else {
        currentRow = 0;
    }
}

void ThumbsViewer::setImageViewerWindowTitle() {
    QString title = thumbsViewerModel->item(currentRow)->data(Qt::DisplayRole).toString()
                    + " - ["
                    + QString::number(currentRow + 1)
                    + "/"
                    + QString::number(thumbsViewerModel->rowCount())
                    + "] - Phototonic";

    phototonic->setWindowTitle(title);
}

bool ThumbsViewer::setCurrentIndexByName(QString &fileName) {
    QModelIndexList indexList = thumbsViewerModel->match(thumbsViewerModel->index(0, 0), FileNameRole, fileName);
    if (indexList.size()) {
        m_currentIndex = indexList[0];
        setCurrentRow(m_currentIndex.row());
        return true;
    }

    return false;
}

bool ThumbsViewer::setCurrentIndexByRow(int row) {
    QModelIndex idx = thumbsViewerModel->indexFromItem(thumbsViewerModel->item(row));
    if (idx.isValid()) {
        m_currentIndex = idx;
        setCurrentRow(idx.row());
        return true;
    }

    return false;
}

void ThumbsViewer::updateImageInfoViewer(int row) {
    QString imageFullPath = thumbsViewerModel->item(row)->data(FileNameRole).toString();
    QImageReader imageInfoReader(imageFullPath);
    QString key;
    QString val;

    QImage img;
    imageInfoReader.read(&img);

    QFileInfo imageInfo = QFileInfo(imageFullPath);
    infoView->addTitleEntry(tr("Image"));

    key = tr("File name");
    val = imageInfo.fileName();
    infoView->addEntry(key, val);

    key = tr("Location");
    val = imageInfo.path();
    infoView->addEntry(key, val);

    key = tr("Size");
    val = QString::number(imageInfo.size() / 1024.0, 'f', 2) + "K";
    infoView->addEntry(key, val);

    key = tr("Modified");
    val = imageInfo.lastModified().toString(Qt::SystemLocaleShortDate);
    infoView->addEntry(key, val);

    if (!img.size().isEmpty()) {
        if (Settings::showHistogram) {
            Histogram hist;
            hist.load(img);
            infoView->setHistogram(hist.render(QSize(infoView->width(), 128)));
        }

        key = tr("Format");
        val = imageInfoReader.format().toUpper();
        infoView->addEntry(key, val);

        key = tr("Resolution");
        val = QString::number(img.size().width())
              + "x"
              + QString::number(img.size().height());
        infoView->addEntry(key, val);

        key = tr("Megapixel");
        val = QString::number((img.size().width() * img.size().height()) / 1000000.0, 'f',
                              2);
        infoView->addEntry(key, val);

        key = tr("Average brightness");
        val = QString::number(thumbsViewerModel->item(row)->data(BrightnessRole).toReal(), 'f', 2);
        infoView->addEntry(key, val);
    } else {
        key = tr("Error");
        val = imageInfoReader.errorString();
        infoView->addEntry(key, val);
    }

    IGNORE_EXIV2_DEPRECATED_START;
    Exiv2::Image::AutoPtr exifImage;
    IGNORE_EXIV2_DEPRECATED_END;

    try {
        exifImage = Exiv2::ImageFactory::open(imageFullPath.toStdString());
        exifImage->readMetadata();
    }
    catch (const Exiv2::Error &error) {
        if (error.code() != Exiv2::kerFileContainsUnknownImageType) {
            qWarning() << "EXIV2:" << error.what();
        }
        return;
    }

    Exiv2::ExifData &exifData = exifImage->exifData();
    if (!exifData.empty()) {
        Exiv2::ExifData::const_iterator end = exifData.end();
        infoView->addTitleEntry("Exif");
        for (Exiv2::ExifData::const_iterator md = exifData.begin(); md != end; ++md) {
            key = QString::fromUtf8(md->tagName().c_str());
            val = QString::fromUtf8(md->print().c_str());
            infoView->addEntry(key, val);
        }
    }

    Exiv2::IptcData &iptcData = exifImage->iptcData();
    if (!iptcData.empty()) {
        Exiv2::IptcData::iterator end = iptcData.end();
        infoView->addTitleEntry("IPTC");
        for (Exiv2::IptcData::iterator md = iptcData.begin(); md != end; ++md) {
            key = QString::fromUtf8(md->tagName().c_str());
            val = QString::fromUtf8(md->print().c_str());
            infoView->addEntry(key, val);
        }
    }

    Exiv2::XmpData &xmpData = exifImage->xmpData();
    if (!xmpData.empty()) {
        Exiv2::XmpData::iterator end = xmpData.end();
        infoView->addTitleEntry("XMP");
        for (Exiv2::XmpData::iterator md = xmpData.begin(); md != end; ++md) {
            key = QString::fromUtf8(md->tagName().c_str());
            val = QString::fromUtf8(md->print().c_str());
            infoView->addEntry(key, val);
        }
    }
}

void ThumbsViewer::onSelectionChanged() {
    infoView->clear();
    imagePreview->clear();
    if (Settings::setWindowIcon && Settings::layoutMode == Phototonic::ThumbViewWidget) {
        phototonic->setWindowIcon(phototonic->getDefaultWindowIcon());
    }

    QModelIndexList indexesList = selectionModel()->selectedIndexes();
    QModelIndex selected = currentIndex();
    int selectedThumbs = indexesList.size();
    if (selected.isValid() || selectedThumbs > 0) {
        int currentRow = 0;
        if (selected.isValid()) {
            currentRow = selected.row();
        } else {
            currentRow = indexesList.first().row();
        }

        QString thumbFullPath = thumbsViewerModel->item(currentRow)->data(FileNameRole).toString();
        setCurrentRow(currentRow);

        if (infoView->isVisible()) {
            updateImageInfoViewer(currentRow);
        }

        QPixmap imagePreviewPixmap = imagePreview->loadImage(thumbFullPath);
        if (Settings::setWindowIcon && Settings::layoutMode == Phototonic::ThumbViewWidget) {
            phototonic->setWindowIcon(imagePreviewPixmap.scaled(WINDOW_ICON_SIZE, WINDOW_ICON_SIZE,
                                                                Qt::KeepAspectRatio, Qt::SmoothTransformation));
        }
    }

    if (imageTags->isVisible() && imageTags->currentDisplayMode == SelectionTagsDisplay) {
        imageTags->showSelectedImagesTags();
    }

    if (selectedThumbs >= 1) {
        QString statusStr;
        statusStr = tr("Selected %1 of %2").arg(QString::number(selectedThumbs))
                .arg(tr(" %n image(s)", "", thumbsViewerModel->rowCount()));
        phototonic->setStatus(statusStr);
    } else if (!selectedThumbs) {
        updateThumbsCount();
    }
}

QStringList ThumbsViewer::getSelectedThumbsList() {
    QModelIndexList indexesList = selectionModel()->selectedIndexes();
    QStringList SelectedThumbsPaths;

    for (int tn = indexesList.size() - 1; tn >= 0; --tn) {
        SelectedThumbsPaths << thumbsViewerModel->item(indexesList[tn].row())->data(FileNameRole).toString();
    }

    return SelectedThumbsPaths;
}

void ThumbsViewer::startDrag(Qt::DropActions) {
    QModelIndexList indexesList = selectionModel()->selectedIndexes();
    if (indexesList.isEmpty()) {
        return;
    }

    QDrag *drag = new QDrag(this);
    QMimeData *mimeData = new QMimeData;
    QList<QUrl> urls;
    for (QModelIndexList::const_iterator it = indexesList.constBegin(),
                 end = indexesList.constEnd(); it != end; ++it) {
        urls << QUrl::fromLocalFile(thumbsViewerModel->item(it->row())->data(FileNameRole).toString());
    }
    mimeData->setUrls(urls);
    drag->setMimeData(mimeData);
    QPixmap pix;
    if (indexesList.count() > 1) {
        pix = QPixmap(128, 112);
        pix.fill(Qt::transparent);
        QPainter painter(&pix);
        painter.setBrush(Qt::NoBrush);
        painter.setPen(QPen(Qt::white, 2));
        int x = 0, y = 0, xMax = 0, yMax = 0;
        for (int i = 0; i < qMin(5, indexesList.count()); ++i) {
            QPixmap pix = thumbsViewerModel->item(indexesList.at(i).row())->icon().pixmap(72);
            if (i == 4) {
                x = (xMax - pix.width()) / 2;
                y = (yMax - pix.height()) / 2;
            }
            painter.drawPixmap(x, y, pix);
            xMax = qMax(xMax, qMin(128, x + pix.width()));
            yMax = qMax(yMax, qMin(112, y + pix.height()));
            painter.drawRect(x + 1, y + 1, qMin(126, pix.width() - 2), qMin(110, pix.height() - 2));
            x = !(x == y) * 56;
            y = !y * 40;
        }
        painter.end();
        pix = pix.copy(0, 0, xMax, yMax);
        drag->setPixmap(pix);
    } else {
        pix = thumbsViewerModel->item(indexesList.at(0).row())->icon().pixmap(128);
        drag->setPixmap(pix);
    }
    drag->setHotSpot(QPoint(pix.width() / 2, pix.height() / 2));
    drag->exec(Qt::CopyAction | Qt::MoveAction | Qt::LinkAction, Qt::IgnoreAction);
}

void ThumbsViewer::abort(bool permanent) {
    isAbortThumbsLoading = true;

    if (!isClosing && permanent) {
        isClosing = true;
    }
}

void ThumbsViewer::loadVisibleThumbs(int scrollBarValue) {

    // Hack:
    // when a paint even is requested Qt first calls updateGeometry() on
    // everything.
    // qscrollbar emits valueChanged() in its updateGeometry(), leading to us
    // possibly recursing when calling processEvents.
    static bool processing = false;
    static int lastScrollBarValue = -1;
    if (processing) {
        updateVisibleThumbnails();
        return;
    }
    processing = true;

    if (reloadVisible) {
        reloadVisible = false;
        loadThumbsRange();
        processing = false;
        return;
    }

    scrolledForward = (scrollBarValue >= lastScrollBarValue);
    lastScrollBarValue = scrollBarValue;

    for (;;) {
        int firstVisible = getFirstVisibleThumb();
        int lastVisible = getLastVisibleThumb();

        // If the timer is started again, it means that what we're doing is outdated.
        // E. g. the user started scrolling in the other direction.
        if (isAbortThumbsLoading || firstVisible < 0 || lastVisible < 0 || m_loadVisibleTimer.isActive()) {
            break;
        }

        if (scrolledForward) {
            lastVisible += (lastVisible - firstVisible) * Settings::thumbsPagesReadCount;
            if (lastVisible >= thumbsViewerModel->rowCount()) {
                lastVisible = thumbsViewerModel->rowCount() - 1;
            }
        } else {
            firstVisible -= (lastVisible - firstVisible) * Settings::thumbsPagesReadCount;
            if (firstVisible < 0) {
                firstVisible = 0;
            }

            lastVisible += 10;
            if (lastVisible >= thumbsViewerModel->rowCount()) {
                lastVisible = thumbsViewerModel->rowCount() - 1;
            }
        }

        if (thumbsRangeFirst == firstVisible && thumbsRangeLast == lastVisible) {
            break;
        }

        thumbsRangeFirst = firstVisible;
        thumbsRangeLast = lastVisible;

        loadThumbsRange();
        if (isAbortThumbsLoading || m_loadVisibleTimer.isActive()) {
            break;
        }
    }
    processing = false;
}

int ThumbsViewer::getFirstVisibleThumb() {
    QModelIndex idx;

    const QRect viewportRect = viewport()->rect();
    for (int currThumb = 0; currThumb < thumbsViewerModel->rowCount(); ++currThumb) {
        idx = thumbsViewerModel->index(currThumb, 0);
        const QRect rect = visualRect(idx);
        if (viewportRect.contains(QPoint(0, rect.bottom() + 1))) {
            return idx.row();
        }
    }

    return -1;
}

int ThumbsViewer::getLastVisibleThumb() {
    QModelIndex idx;

    const QRect viewportRect = viewport()->rect();
    for (int currThumb = thumbsViewerModel->rowCount() - 1; currThumb >= 0; --currThumb) {
        idx = thumbsViewerModel->index(currThumb, 0);

        const QRect rect = visualRect(idx);
        if (viewportRect.contains(QPoint(0, rect.bottom() + 1))) {
            return idx.row();
        }
    }

    return -1;
}

void ThumbsViewer::loadFileList() {
    for (int i = 0; i < Settings::filesList.size(); i++) {
        addThumb(Settings::filesList[i]);
    }
    updateThumbsCount();

    imageTags->populateTagsTree();

    if (thumbFileInfoList.size() && selectionModel()->selectedIndexes().size() == 0) {
        selectThumbByRow(0);
    }

    phototonic->showBusyAnimation(false);
    isBusy = false;
}

void ThumbsViewer::refreshThumbnails(bool clearContents) {
    const QSize hintSize = itemSizeHint();
    for (int i=0; i<thumbsViewerModel->rowCount(); i++) {
        QStandardItem *item = thumbsViewerModel->item(i);
        item->setSizeHint(hintSize);

        if (!clearContents) {
            continue;
        }
        if (!item->data(LoadedRole).toBool()) {
            continue;
        }
        item->setIcon(QIcon());
        item->setData(false, LoadedRole);
    }
    updateVisibleThumbnails();
}

void ThumbsViewer::reLoad() {
    isBusy = true;
    phototonic->showBusyAnimation(true);
    resetModel();

    failedFiles.clear();

    updateItemLayout();

    if (Settings::isFileListLoaded) {
        loadFileList();
        return;
    }

    applyFilter();
    updateThumbsCount(true);
    thumbFileInfoList = thumbsDir.entryInfoList();
    initThumbs();
    imageTags->populateTagsTree();
    updateThumbsCount();

    updateVisibleThumbnails();

    if (Settings::includeSubDirectories) {
        loadSubDirectories();
    }

    phototonic->showBusyAnimation(false);
    isBusy = false;
}

void ThumbsViewer::loadSubDirectories() {
    QDirIterator dirIterator(Settings::currentDirectory.absoluteFilePath(), QDirIterator::Subdirectories);

    int processed = 0;
    while (dirIterator.hasNext()) {
        dirIterator.next();
        if (dirIterator.fileInfo().isDir() && dirIterator.fileName() != "." && dirIterator.fileName() != "..") {
            thumbsDir.setPath(dirIterator.filePath());
            updateThumbsCount(true);
            thumbFileInfoList = thumbsDir.entryInfoList();
            if (!thumbFileInfoList.isEmpty()) {
                initThumbs();
                updateThumbsCount();

                updateVisibleThumbnails();
            }

            if (isAbortThumbsLoading) {
                return;
            }
        }
        if (++processed > BATCH_SIZE) {
            if (imageTags->isVisible()) {
                imageTags->populateTagsTree();
            }
            QApplication::processEvents();
            processed = 0;
        }
    }
    imageTags->populateTagsTree();

    onSelectionChanged();
}

void ThumbsViewer::applyFilter() {
    fileFilters.clear();

    // Get all patterns supported by QImageReader
    static QStringList imageTypeGlobs;
    // Not threadsafe, but whatever
    if (imageTypeGlobs.isEmpty()) {
        QMimeDatabase db;
        for (const QByteArray &type : QImageReader::supportedMimeTypes()) {
            imageTypeGlobs.append(db.mimeTypeForName(type).globPatterns());
        }
    }
    for (const QString &glob : imageTypeGlobs) {
        fileFilters.append(glob);
    }

    thumbsDir.setNameFilters(imageTypeGlobs);
    thumbsDir.setFilter(QDir::Files);
    if (Settings::showHiddenFiles) {
        thumbsDir.setFilter(thumbsDir.filter() | QDir::Hidden);
    }

    thumbsDir.setPath(Settings::currentDirectory.absoluteFilePath());
    QDir::SortFlags tempThumbsSortFlags = thumbsSortFlags;
    if (tempThumbsSortFlags & QDir::Size || tempThumbsSortFlags & QDir::Time) {
        tempThumbsSortFlags ^= QDir::Reversed;
    }

    if (thumbsSortFlags & QDir::Time || thumbsSortFlags & QDir::Size || thumbsSortFlags & QDir::Type) {
        thumbsDir.setSorting(tempThumbsSortFlags);
    } else { // by name
        thumbsDir.setSorting(QDir::NoSort);
    }
}

QSize ThumbsViewer::itemSizeHint() const
{
    switch(Settings::thumbsLayout) {
    case Squares:
        return QSize(thumbSize, thumbSize);
    case Compact:
        return QSize(thumbSize, thumbSize + ((int) (QFontMetrics(font()).height() * 2.5)));
    case Classic:
        return QSize(thumbSize, thumbSize + ((int) (QFontMetrics(font()).height() * 2.5)));
    default:
        qWarning() << "Invalid thumbs layout" << Settings::thumbsLayout;
        return QSize(thumbSize, thumbSize);
    }
}

void ThumbsViewer::resetModel() {
    thumbsViewerModel->clear();

    rootHashIndex = -1;
    rootHistIndex = -1;

    imageTags->resetTagsState();

    if (!isClosing) {
        isAbortThumbsLoading = false;
    }
}

void ThumbsViewer::updateItemLayout() {
    QModelIndex toPreserveVisible = currentIndex();
    if (toPreserveVisible.isValid()) {
        if (!viewport()->rect().contains(visualRect(toPreserveVisible))) {
            toPreserveVisible = QModelIndex();
        }
    }

    if (!toPreserveVisible.isValid()) {
        const int row = getFirstVisibleThumb();
        toPreserveVisible = thumbsViewerModel->index(row, 0);
    }

    setIconSize(QSize(thumbSize, thumbSize));

    if (Settings::thumbsLayout == Squares) {
        setSpacing(0);
        setUniformItemSizes(true);
        setGridSize(itemSizeHint());
    } else if (Settings::thumbsLayout == Compact) {
        setSpacing(0);
        setUniformItemSizes(true);
        setGridSize(itemSizeHint());
    } else {
        setSpacing(QFontMetrics(font()).height());
        setUniformItemSizes(true);
        setGridSize(QSize());
    }

    if (isNeedToScroll) {
        scrollToTop();
    } else if (toPreserveVisible.isValid()) {
        scrollTo(toPreserveVisible);
    }

    thumbsRangeFirst = -1;
    thumbsRangeLast = -1;
}

void ThumbsViewer::loadDuplicates()
{
    isBusy = true;
    phototonic->showBusyAnimation(true);


    phototonic->setStatus(tr("Searching duplicate images..."));
    resetModel();
    updateItemLayout();

    {
        QProgressDialog progress(tr("Loading images..."), tr("Abort"), 0, thumbFileInfoList.count(), this);
        ensureHistogramsAndHashes(&progress);
        if (progress.wasCanceled()) {
            goto finish;
        }
    }

    dupImageHashes.clear();
    rootHash = 0;

    findDupes(true);
    thumbsViewerModel->setSortRole(DuplicateOrder);

    if (Settings::includeSubDirectories) {
        int processed = 0;
        QDirIterator iterator(Settings::currentDirectory.absoluteFilePath(), QDirIterator::Subdirectories);
        while (iterator.hasNext()) {
            iterator.next();
            if (iterator.fileInfo().isDir() && iterator.fileName() != "." && iterator.fileName() != "..") {
                thumbsDir.setPath(iterator.filePath());

                findDupes(false);
                if (isAbortThumbsLoading) {
                    goto finish;
                }
            }
            if (++processed > BATCH_SIZE) {
                QApplication::processEvents();
                processed = 0;
            }
        }
    }

finish:
    isBusy = false;
    phototonic->showBusyAnimation(false);
    return;
}

void ThumbsViewer::initThumbs() {
    phototonic->showBusyAnimation(true);

    if (!(thumbsSortFlags & QDir::Time) && !(thumbsSortFlags & QDir::Size) && !(thumbsSortFlags & QDir::Type)) {
        QCollator collator;
        if (thumbsSortFlags & QDir::IgnoreCase) {
            collator.setCaseSensitivity(Qt::CaseInsensitive);
        }

        collator.setNumericMode(true);

        if (thumbsSortFlags & QDir::Reversed) {
            std::sort(thumbFileInfoList.begin(), thumbFileInfoList.end(), [&](const QFileInfo &a, const QFileInfo &b) {
                    return collator.compare(a.fileName(), b.fileName()) > 0;
                    });
        } else {
            std::sort(thumbFileInfoList.begin(), thumbFileInfoList.end(), [&](const QFileInfo &a, const QFileInfo &b) {
                    return collator.compare(a.fileName(), b.fileName()) < 0;
                    });
        }
    }

    static QStandardItem *thumbItem;
    static int fileIndex;
    static QPixmap emptyPixMap;
    int processed = 0;

    emptyPixMap = emptyImg.scaled(thumbSize, thumbSize);

    QElapsedTimer t; t.start();
    for (fileIndex = 0; fileIndex < thumbFileInfoList.size(); ++fileIndex) {
        thumbFileInfo = thumbFileInfoList.at(fileIndex);

        metadataCache->loadImageMetadata(thumbFileInfo.filePath());
        if (imageTags->dirFilteringActive && imageTags->isImageFilteredOut(thumbFileInfo.filePath())) {
            continue;
        }

        nameSortValues[thumbFileInfo.absoluteFilePath()] = fileIndex;

        thumbItem = new QStandardItem();
        thumbItem->setData(false, LoadedRole);
        thumbItem->setData(qreal(fileIndex), SortRole);
        thumbItem->setData(thumbFileInfo.size(), SizeRole);
        thumbItem->setData(thumbFileInfo.suffix(), TypeRole);
        thumbItem->setData(thumbFileInfo.lastModified(), TimeRole);
        thumbItem->setData(thumbFileInfo.filePath(), FileNameRole);
        const QSize resolution = metadataCache->getResolution(thumbFileInfo.filePath());
        thumbItem->setData(resolution.width() * resolution.height(), ResolutionRole);
        thumbItem->setSizeHint(itemSizeHint());

        if (Settings::thumbsLayout != Squares) {
            thumbItem->setTextAlignment(Qt::AlignCenter);
            thumbItem->setText(thumbFileInfo.fileName());
        }

        thumbsViewerModel->appendRow(thumbItem);

        if (++processed > BATCH_SIZE || t.elapsed() > 100) {
            QApplication::processEvents();
            processed = 0;
            t.restart();
        }
    }

    if (thumbFileInfoList.size() && selectionModel()->selectedIndexes().size() == 0) {
        selectThumbByRow(0);
    }
    phototonic->showBusyAnimation(false);
}

void ThumbsViewer::updateThumbsCount(const bool scanning) {
    QString state;

    if (scanning) {
        if (thumbsViewerModel->rowCount() > 0) {
            state = tr("%n image(s), scanning...", "", thumbsViewerModel->rowCount());
        } else {
            state = tr("No images, scanning...");
        }
    } else {
        if (thumbsViewerModel->rowCount() > 0) {
            state = tr("%n image(s)", "", thumbsViewerModel->rowCount());
        } else {
            state = tr("No images");
        }
        thumbsDir.setPath(Settings::currentDirectory.absoluteFilePath());
    }
    phototonic->setStatus(state);
}

void ThumbsViewer::selectThumbByRow(int row) {
    setCurrentIndexByRow(row);
    selectCurrentIndex();
}

void ThumbsViewer::updateFoundDupesState(int duplicates, int filesScanned, int originalImages)
{
    QString state;
    state = tr("Scanned %1, displaying %2 (%3 and %4)")
                .arg(tr("%n image(s)", "", filesScanned))
                .arg(tr("%n image(s)", "", originalImages + duplicates))
                .arg(tr("%n original(s)", "", originalImages))
                .arg(tr("%n duplicate(s)", "", duplicates));
    phototonic->setStatus(state);
}

void ThumbsViewer::findDupes(bool resetCounters)
{
    thumbFileInfoList = thumbsDir.entryInfoList();
    static int originalImages;
    static int foundDups;
    static int totalFiles;
    if (resetCounters) {
        originalImages = totalFiles = foundDups = 0;
    }

    int processed = 0;

    ensureHistogramsAndHashes(nullptr);
    if (isAbortThumbsLoading) {
        return;
    }

    for (int currThumb = 0; currThumb < thumbFileInfoList.size(); ++currThumb) {
        if (++processed > BATCH_SIZE) {
            thumbsViewerModel->sort(0);
            QApplication::processEvents();
            processed = 0;
        }

        thumbFileInfo = thumbFileInfoList.at(currThumb);
        uint64_t imageHash = 0;
        const QString filePath = thumbFileInfo.absoluteFilePath();
        if (failedFiles.contains(filePath)) {
            continue;
        }

        int hashIndex = sortedFiles.indexOf(filePath);
        if (hashIndex != -1) {
            imageHash = imageHashes[hashIndex];
        } else {
            analyzeImage(filePath, loadThumbImage(filePath, ANALYZE_SIZE, false));
            hashIndex = sortedFiles.indexOf(filePath);
            imageHash = imageHashes[hashIndex];
        }

        if (hashIndex == -1) {
            qWarning() << "Something went very wrong";
            continue;
        }

        const QString currentFilePath = thumbFileInfo.filePath();

        totalFiles++;

        if (dupImageHashes.contains(imageHash)) {
            DuplicateImage &dup = dupImageHashes[imageHash];

            if (dup.duplicates < 1) {
                dup.id = foundDups;
                // Since we also sort by similarity, there's a higher chance that almost-duplicates show up together
                dup.sortOrder = 1. + rootHist.compare(histograms[hashIndex]);
                dup.sortOrder *=  1. + POPCNT(rootHash  ^ imageHash);
                dup.sortOrder = std::round(dup.sortOrder * 10000) * 100;
                dup.sortOrder += dup.id;

                QStandardItem *item = addThumb(dup.filePath, dup.sortOrder);
                if (item) {
                    item->setData(dup.sortOrder, DuplicateOrder);
                }

                originalImages++;
            }

            foundDups++;
            dup.duplicates++;
            QStandardItem *item = addThumb(currentFilePath, dup.sortOrder);
            if (item) {
                item->setData(dup.sortOrder, DuplicateOrder);
            }
        } else {
            if (currentFilePath.isEmpty()) {
                qWarning() << "Empty file path!";
            }
            DuplicateImage dupImage;
            dupImage.filePath = currentFilePath;
            dupImage.duplicates = 0;
            dupImage.id = dupImageHashes.count();
            dupImageHashes.insert(imageHash, dupImage);
        }

        updateFoundDupesState(foundDups, totalFiles, originalImages);

        if (isAbortThumbsLoading) {
            break;
        }
    }

    updateFoundDupesState(foundDups, totalFiles, originalImages);
}

void ThumbsViewer::selectByBrightness(qreal min, qreal max) {
    loadAllThumbs();
    QItemSelection sel;
    for (int row = 0; row < thumbsViewerModel->rowCount(); ++row) {
        QModelIndex idx = thumbsViewerModel->index(row, 0);
        QVariant brightness = thumbsViewerModel->data(idx, BrightnessRole);
        if (brightness.isValid()) {
            qreal val = brightness.toReal();
            if (val >= min && val <= max)
                sel.select(idx, idx);
        }
    }
    selectionModel()->select(sel, QItemSelectionModel::ClearAndSelect);
}

void ThumbsViewer::loadAllThumbs() {
    QProgressDialog progress(tr("Loading thumbnails..."), tr("Abort"), 0, thumbFileInfoList.count(), this);
    int processed = 0;
    for (int i = 0; i < thumbFileInfoList.count(); ++i) {
        progress.setValue(i);
        if (progress.wasCanceled())
            break;
        if (thumbsViewerModel->item(i)->data(LoadedRole).toBool())
            continue;
        loadThumb(i);

        if (isAbortThumbsLoading) {
            break;
        }

        if (++processed > BATCH_SIZE) {
            QApplication::processEvents();
            processed = 0;
        }
    }
}

void ThumbsViewer::ensureHistogramsAndHashes(QProgressDialog *progress) {
    int processed = 0;
    QStringList files;
    for (const QFileInfo &fi : thumbFileInfoList) {
        QString filename = fi.absoluteFilePath();
        if (filename.isEmpty() || loadedSortedFiles.contains(filename) || failedFiles.contains(filename)) {
            continue;
        }
        files.append(filename);
    }
    const int rows = thumbsViewerModel->rowCount();// thumbFileInfoList.count();
    for (int i = 0; i < rows; ++i) {
        QString filename = thumbsViewerModel->index(i, 0).data(FileNameRole).toString();
        if (filename.isEmpty() || loadedSortedFiles.contains(filename) || failedFiles.contains(filename)) {
            continue;
        }
        files.append(filename);
    }

    files.removeDuplicates();

    if (progress) {
        progress->setMaximum(files.count());
    }

    qint64 totalLoad = 0;
    int loaded = 0;
    for (const QString &filename : files) {
        loaded++;
        QElapsedTimer t; t.start();

        if (loadedSortedFiles.contains(filename) || failedFiles.contains(filename)) {
            totalLoad += t.elapsed();
            continue;
        }

        analyzeImage(filename, loadThumbImage(filename, ANALYZE_SIZE, false));

        if (++processed > BATCH_SIZE) {
            processed = 0;
            if (progress) {
                if (!progress->isVisible()) {
                    progress->show();
                }
                progress->setValue(loaded);
            } else {
                phototonic->setStatus(tr("Loaded %1/%2 image files...").arg(loaded).arg(files.count()));
            }
            QApplication::processEvents();
            if (isAbortThumbsLoading || (progress && progress->wasCanceled())) {
                qDebug() << "Aborted";
                totalLoad += t.elapsed();
                break;
            }
        }
        totalLoad += t.elapsed();
    }

    if (rootHashIndex == -1 || rootHistIndex == -1) {
        QStringList selected = getSelectedThumbsList();
        // if it is empty the loop will never run
        for (const QString &file : selected) {
            if (!loadedSortedFiles.contains(file) || failedFiles.contains(file)) {
                // shouldn't be possible, but whaterver
                continue;
            }

            rootHashIndex = sortedFiles.indexOf(file);

            if (rootHashIndex == -1) {
                // shouldn't be possible either
                continue;
            }

            rootHash = imageHashes[rootHashIndex];

            rootHistIndex = rootHashIndex;
            rootHist = histograms[rootHistIndex];
            break;
        }
    }

    if (rootHashIndex == -1) {
        qWarning() << "Failed to find selected items, finding a good one";

        QElapsedTimer t; t.start();
        int bestBitCount = 64;

        rootHashIndex = 0;
        for (int i=0; i<imageHashes.count(); i++) {
            const uint64_t bits = imageHashes[i];
            const int distance = qAbs(POPCNT(bits) - 32);//qAbs(bits.count(true) - 32);
            if (distance < bestBitCount) {
                rootHash = bits;
                bestBitCount = distance;
                rootHashIndex = i;
            }
            if (distance >= 31) {
                break;
            }
        }
    }

    if (rootHistIndex == -1) {
        float bestScore = std::numeric_limits<float>::max();
        Histogram empty;
        for (int i=0; i<256; i++) {
            empty.red[i] = 1.;
            empty.green[i] = 1.;
            empty.blue[i] = 1.;
        }

        rootHistIndex = 0;
        for (int i=0; i<histograms.size(); i++) {
            const Histogram &histogram = histograms[i];
            const float score = histogram.compare(empty) +
                    histogram.compareChannel(histogram.blue, histogram.red) +
                    histogram.compareChannel(histogram.green, histogram.red) +
                    histogram.compareChannel(histogram.blue, histogram.green);
            if (std::isnan(score)) {
                continue;
            }

            if (score > bestScore) {
                continue;
            }

            rootHistIndex = i;
            rootHist = histogram;
            bestScore = score;
        }
    }
    if (qEnvironmentVariableIsSet("PHOTOTONIC_BENCH") && totalLoad > 0) {
        qDebug() << "Avg load time" << totalLoad / files.count() << "total" << totalLoad << "for" << files.count() << "images";
    }
}

void ThumbsViewer::sortBySimilarity(const SimilarityMode mode) {
    QElapsedTimer t; t.start();
    QProgressDialog progress(tr("Loading..."), tr("Abort"), 0, thumbFileInfoList.count(), this);
    progress.show();
    QApplication::processEvents();

    rootHistIndex = -1;
    rootHashIndex = -1;
    ensureHistogramsAndHashes(&progress);

    if (progress.wasCanceled() || isAbortThumbsLoading) {
        return;
    }
    if (qEnvironmentVariableIsSet("PHOTOTONIC_BENCH") && t.elapsed()) {
        qDebug() << "Loaded in" << t.elapsed() << "ms";
    }

    progress.setLabelText(tr("Comparing..."));
    progress.setValue(0);
    int processed = 0;

    const int rootIndex = mode == ColorSimilarity ? rootHistIndex : rootHashIndex;
    if (rootIndex >= 0 && rootIndex < sortedFiles.size()) {
        std::swap(sortedFiles[0], sortedFiles[rootIndex]);
        std::swap(histograms[0], histograms[rootIndex]);
        std::swap(imageHashes[0], imageHashes[rootIndex]);
    }

    t.restart();
    QMap<QString, qreal> scores;
    for (int i=0; i<sortedFiles.size() - 1; i++) {
        float minScore = std::numeric_limits<float>::max();
        int minIndex = i+1;

        if (mode == ColorSimilarity) {
            for (int j=i+1; j<sortedFiles.size(); j++) {
                const float score = histograms[i].compare(histograms[j]);
                if (score > minScore) {
                    continue;
                }
                minIndex = j;
                minScore = score;
            }
        } else {
            for (int j=i+1; j<sortedFiles.size(); j++) {
                const float score = histograms[i].compare(histograms[j]) *
                    POPCNT(imageHashes[i]  ^ imageHashes[j]);
                if (score > minScore) {
                    continue;
                }
                minIndex = j;
                minScore = score;
            }
        }

        processed += sortedFiles.size() - (i + 1);

        std::swap(sortedFiles[i+1], sortedFiles[minIndex]);
        std::swap(histograms[i+1], histograms[minIndex]);
        std::swap(imageHashes[i+1], imageHashes[minIndex]);

        if (processed > BATCH_SIZE * 10) {
            processed = 0;
            progress.show();
            progress.setValue(i);
            QApplication::processEvents();
            if (progress.wasCanceled()) {
                return;
            }
        }
    }
    if (qEnvironmentVariableIsSet("PHOTOTONIC_BENCH") && t.elapsed()) {
        qDebug() << "Compared in" << t.elapsed() << "ms";
    }

    progress.setLabelText(tr("Sorting..."));
    progress.setMaximum(thumbFileInfoList.count() + 1); // + 1 for the call to sort() at the bottom
    progress.setValue(0);

    t.restart();
    QHash<QString, int> indices;
    for (int i=0; i<sortedFiles.size(); i++) {
        indices[sortedFiles[i]] = i;
    }
    for (int i = 0; i < thumbsViewerModel->rowCount(); ++i) {
        QStandardItem *item = thumbsViewerModel->item(i);
        Q_ASSERT(item);
        if (!item) {
            qWarning() << "Invalid item" << i;
            continue;
        }
        const QString filename = item->data(FileNameRole).toString();
        if (!indices.contains(filename)) {
            qWarning() << "Invalid file" << filename;
            item->setData(-1, SortRole);
            item->setData(-1, SimilarityRole);
            continue;
        }

        item->setData(indices.size() - indices[filename], SimilarityRole);

        if (++processed > BATCH_SIZE) {
            processed = 0;
            progress.show();
            progress.setValue(i);
            QApplication::processEvents();
            if (progress.wasCanceled()) {
                return;
            }
        }
    }
    if (qEnvironmentVariableIsSet("PHOTOTONIC_BENCH") && t.elapsed()) {
        qDebug() << "Sorted in" << t.elapsed() << "ms";
    }
    QApplication::processEvents();

    thumbsViewerModel->setSortRole(SimilarityRole);
    thumbsViewerModel->sort(0, Qt::DescendingOrder);
}

void ThumbsViewer::loadThumbsRange() {
    static bool isInProgress = false;
    static int currentRowCount;
    int currThumb;

    if (isInProgress) {
        isAbortThumbsLoading = true;
        QTimer::singleShot(0, this, SLOT(loadThumbsRange()));
        return;
    }

    isInProgress = true;
    currentRowCount = thumbsViewerModel->rowCount();

    QElapsedTimer timer;
    timer.start();

    for (scrolledForward ? currThumb = thumbsRangeFirst : currThumb = thumbsRangeLast;
         (scrolledForward ? currThumb <= thumbsRangeLast : currThumb >= thumbsRangeFirst);
         scrolledForward ? ++currThumb : --currThumb) {

        if (isAbortThumbsLoading || thumbsViewerModel->rowCount() != currentRowCount || currThumb < 0)
            break;

        QStandardItem *currItem = thumbsViewerModel->item(currThumb);
        if (!currItem) {
            QTimer::singleShot(0, this, SLOT(loadThumbsRange()));
            break;
        }
        if (currItem->data(LoadedRole).toBool())
            continue;

        loadThumb(currThumb);

        if (timer.elapsed() > 10) {
            QApplication::processEvents();
            timer.restart();
        }
    }

    isInProgress = false;

    if (!isClosing) {
        isAbortThumbsLoading = false;
    }
}

QString ThumbsViewer::thumbnailFileName(const QString &originalPath) const
{
    QFileInfo info(originalPath);
    QString canonicalPath = info.canonicalFilePath();
    if (canonicalPath.isEmpty()) {
        qWarning() << originalPath << "does not exist!";
        canonicalPath = info.absoluteFilePath();
    }
    QUrl url = QUrl::fromLocalFile(canonicalPath);
    QCryptographicHash md5(QCryptographicHash::Md5);
    md5.addData(QFile::encodeName(url.adjusted(QUrl::RemovePassword).url()));
    return QString::fromLatin1(md5.result().toHex()) + QStringLiteral(".png");
}

static bool isAcceptableSize(const QSize &size, const QSize &targetSize, const bool shrinkable)
{
    if (size.width() < targetSize.width() && size.height() < targetSize.height()) {
        return false;
    }

    if (size.width() > targetSize.width() && size.height() > targetSize.height()) {
        return true;
    }

    // Need bigger than entire target size if not shrinkable
    if (!shrinkable) {
        return false;
    }

    return true;
}

QString ThumbsViewer::locateThumbnail(const QString &originalPath, const QSize &targetSize, const bool shrinkable) const
{
#if defined(Q_OS_MAC) || defined(Q_OS_WIN)
    return "";
#endif
    QStringList folders;

    int minSize = targetSize.width();
    if (shrinkable) {
        minSize = qMin(minSize, targetSize.height());
    }

    if (minSize <= 128) {
        folders.append(QStringLiteral("normal/")); // 128px max
    }
    if (minSize <= 256) {
        folders.append(QStringLiteral("large/")); // max 256px
    }
    if (minSize <= 512) {
        folders.append(QStringLiteral("x-large/")); // max 512px
    }
    folders.append(QStringLiteral("xx-large/")); // max 1024px

    const QString filename = thumbnailFileName(originalPath);
    const QString basePath = QStandardPaths::writableLocation(QStandardPaths::GenericCacheLocation) +
        QLatin1String("/thumbnails/");
    const QFileInfo originalInfo(originalPath);
    for (const QString &folder : folders) {
        QFileInfo info(basePath + folder + filename);
        if (!info.exists()) {
            continue;
        }
        if (originalInfo.metadataChangeTime() > info.lastModified()) {
            continue;
        }
        if (originalInfo.lastModified() > info.lastModified()) {
            continue;
        }
        QImageReader reader(info.absoluteFilePath());
        const QSize size = reader.size();
        // Need bigger than entire target size if not shrinkable
        if (!isAcceptableSize(size, targetSize, shrinkable)) {
            continue;
        }
        return info.absoluteFilePath();
    }
    return QString();
}

void ThumbsViewer::storeThumbnail(const QString &originalPath, QImage thumbnail, const QSize &originalSize) const {
#if defined(Q_OS_MAC) || defined(Q_OS_WIN)
    return;
#endif
    QFileInfo info(originalPath);
    const QString canonicalPath = info.canonicalFilePath();
    if (canonicalPath.isEmpty()) {
        qWarning() << "Asked to store thumbnail for non-existent path" << originalPath;
        return;
    }

    QString folder = QStringLiteral("normal/");
    const int maxSize = qMax(thumbnail.width(), thumbnail.height());
    if (maxSize < 64) {
        qDebug() << "Refusing to store tiny thumbnail" << thumbnail.size();
        return;
    }

    if (maxSize >= 512) {
        folder = QStringLiteral("xx-large/");
        thumbnail = thumbnail.scaled(1024, 1024, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    } else if (maxSize >= 256) { // x-large is max 512
        folder = QStringLiteral("x-large/");
    } else if (maxSize >= 128) { // large is max 256
        folder = QStringLiteral("large/");
    } else if (maxSize >= 64) {
        folder = QStringLiteral("normal/");
    } else {
        qWarning() << "Thumbnail too small" << thumbnail.size();
        return;
    }

    const QString filename = thumbnailFileName(originalPath);
    const QString basePath = QStandardPaths::writableLocation(QStandardPaths::GenericCacheLocation) +
        QLatin1String("/thumbnails/");

    if (!QFileInfo::exists(basePath + folder)) {
        QDir().mkpath(basePath + folder);
    }

    const QString fullPath = basePath + folder + filename;

    QDateTime lastModified = info.lastModified();
    if (info.metadataChangeTime() > info.lastModified()) {
        lastModified = info.metadataChangeTime();
    }
    thumbnail.setText(QStringLiteral("Thumb::MTime"), QString::number(lastModified.toSecsSinceEpoch()));

    QUrl url = QUrl::fromLocalFile(canonicalPath).adjusted(QUrl::RemovePassword);
    thumbnail.setText(QStringLiteral("Thumb::URI"), url.url());

    thumbnail.setText("Thumb::Size", QString::number(info.size()));

    thumbnail.setText(QStringLiteral("Thumb::Image::Width"), QString::number(originalSize.width()));
    thumbnail.setText(QStringLiteral("Thumb::Image::Height"), QString::number(originalSize.height()));
    thumbnail.setText("Software", "Phototonic");
    thumbnail.convertToColorSpace(QColorSpace::SRgb);

    // 64 bit images hits a slow path in qt and/or libpng, so avoid storing that
    if (thumbnail.depth() > 32) {
        thumbnail = thumbnail.convertToFormat(thumbnail.hasAlphaChannel() ? QImage::Format_ARGB32 : QImage::Format_RGB32);
    }

    // 81 maps to 8 compression via Qt's magic calculation, libpng goes from 0
    // to 9, where 9 is lossless.
    // 8 is still about 2-3 times as slow as 9, but it gives 1/3 the image size
    // for x-large thumbnails, so it's an okay tradeoff.
    // Anything less than 8 is useless for thumbnails since they are so small.
    if (!thumbnail.save(fullPath, "PNG", 81)) {
        qWarning() << "Failed to save to" << fullPath;
    }
}

static bool isValidThumbnail(const QImage &thumbImage, const QString &originalFilename, const QSize &originalDimensions)
{
    if (thumbImage.isNull()) {
        return false;
    }

    // This is cheapest to check, we already have all info and don't need to hit the disk
    const QStringList keys = thumbImage.textKeys();
    if (keys.contains("Thumb::Image::Width") && keys.contains("Thumb::Image::Width") && !originalDimensions.isEmpty()) {
        int w = thumbImage.text("Thumb::Image::Width").toInt();
        int h = thumbImage.text("Thumb::Image::Height").toInt();
        const QSize fullSize(w, h);
        if (!originalDimensions.isEmpty() && !fullSize.isEmpty()) {
            if (w != originalDimensions.width() || h != originalDimensions.height()) {
                return false;
            }
        }

        if (fullSize.isEmpty()) {
            return false;
        }
    }

    QFileInfo fi(originalFilename);
    if (keys.contains("Thumb::MTime")) {
        const long long timeSinceEpoch = thumbImage.text("Thumb::MTime").toLongLong();
        const long long fileLastModified = qMax(fi.metadataChangeTime().toSecsSinceEpoch(), fi.lastModified().toSecsSinceEpoch());
        if (timeSinceEpoch != fileLastModified) {
            qWarning() << "Invalid last modified" << timeSinceEpoch << fileLastModified << fi.metadataChangeTime().toSecsSinceEpoch() << fi.lastModified().toSecsSinceEpoch();
            const bool fromKDE = thumbImage.text("Software").startsWith("KDE Thumbnail Generator Images");

            // KDE is broken, so avoid us overwriting eachothers thumbnails
            if (!fromKDE || timeSinceEpoch != fi.lastModified().toSecsSinceEpoch()) {
                return false;
            }
        }
    }

    if (keys.contains("Thumb::Size")) {
        const long long size = thumbImage.text("Thumb::Size").toLongLong();
        if (size != fi.size()) {
            qWarning() << "Invalid file size in thumbnail" << size << thumbImage.text("Thumb::Size");
            return false;
        }
    }

    return true;
}

QImage ThumbsViewer::loadThumbImage(const QString &imageFileName, const int targetSize, bool shrinkable) {
    QImageReader sourceReader;

    sourceReader.setFileName(imageFileName);
    sourceReader.setQuality(50); // 50 is the threshold where Qt does fast decoding, but still good scaling

    if (!sourceReader.canRead()) {
        failedFiles.insert(imageFileName);
        return {};
    }

    QSize sourceSize;

    // Certain image format plugins decode the entire image when queried for
    // the size...
    static const QSet<QString> blacklistedDecoders = {
        "heif",
        "svg",
        "pdf"
    };
    if (!blacklistedDecoders.contains(sourceReader.format())) {
        sourceSize = sourceReader.size();
    }

    QSize outputSize = sourceSize;

    if (!outputSize.isEmpty()) {
        if (outputSize.width() < 64 && outputSize.width() < 64) {
            QImage image;
            sourceReader.read(&image);
            return image;
        }
        if (outputSize.width() > targetSize || outputSize.height() > targetSize) {
            outputSize.scale(QSize(targetSize, targetSize), shrinkable ?  Qt::KeepAspectRatio : Qt::KeepAspectRatioByExpanding);
        }
        sourceReader.setScaledSize(outputSize);
    } else {
        outputSize = QSize(targetSize, targetSize);
    }

    QString thumbnailPath = locateThumbnail(imageFileName, outputSize, shrinkable);
    if (!thumbnailPath.isEmpty()) {
        QImageReader thumbReader;
        thumbReader.setFileName(thumbnailPath);

        // avoid weird scaling
        if (!sourceSize.isEmpty()) {
            thumbReader.setScaledSize(outputSize);
        }

        QImage thumb;
        thumbReader.read(&thumb);

        if (isValidThumbnail(thumb, imageFileName, sourceSize)) {
            if (isAcceptableSize(thumb.size(), outputSize, shrinkable)) {
                return thumb;
            }

            if (thumb.size().width() >= outputSize.width() && thumb.size().height() >= outputSize.height()) {
                return thumb;
            }
        } else {
            if (!thumb.isNull()) {
                qWarning() << "Invalid thumbnail from software" << thumb.text("Software") << thumb.size();
            }
            qWarning() << "invalid thumbnail loaded, output size:" << outputSize << "thumb size" << thumb.size() << "original size" << sourceSize;
            qDebug() << "For file" << imageFileName;
        }
    }

    QImage sourceImage;
    sourceReader.read(&sourceImage);

    // try again, without scaled size
    // We need to create a new reader, because that's how QImageReader works
    if (sourceImage.size().isEmpty() || !isAcceptableSize(sourceImage.size(), outputSize, shrinkable)) {
        QImageReader newReader(imageFileName);
        newReader.read(&sourceImage);

        // completely failed
        if (sourceImage.size().isEmpty()) {
            qWarning() << "Completely failed to load" << QFileInfo(imageFileName).fileName();
            failedFiles.insert(imageFileName);
            return sourceImage;
        }
    }

    if (sourceImage.isNull()) {
        failedFiles.insert(imageFileName);
        return {};
    }
    if (sourceSize.isEmpty()) {
        // only set if it isn't empty, if it's not empty we set scaled size when loading and this is wrong
        sourceSize = sourceImage.size();
    }

    QImage thumb;
    if (sourceImage.width() > targetSize || sourceImage.height() > targetSize) {
        thumb = sourceImage.scaled(QSize(targetSize, targetSize), shrinkable ?  Qt::KeepAspectRatio : Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
    } else {
        thumb = sourceImage;
    }

    storeThumbnail(imageFileName, thumb, sourceSize);

    return sourceImage;
}

void ThumbsViewer::analyzeImage(const QString &filename, const QImage &inputImage) {
    if (loadedSortedFiles.contains(filename)) {
        return;
    }
    if (inputImage.isNull()) {
        failedFiles.insert(filename);
        return;
    }
    failedFiles.remove(filename);

    QImage image = inputImage;

    if (inputImage.width() != ANALYZE_SIZE || inputImage.height() != ANALYZE_SIZE) {
        image = inputImage.scaled(ANALYZE_SIZE, ANALYZE_SIZE, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    }

    switch(inputImage.format()) {
    case QImage::Format_ARGB32:
    case QImage::Format_RGB32:
        break;
    case QImage::Format_ARGB32_Premultiplied:
    default:
        image = image.convertToFormat(QImage::Format_RGB32);
        break;
    }

    imageHashes.append(calcImageHash(image, filename));
    histograms.append(calcHist(image));
    sortedFiles.append(filename);
    loadedSortedFiles.insert(filename);
}

bool ThumbsViewer::loadThumb(int currThumb) {
    QStandardItem *item = thumbsViewerModel->item(currThumb);
    if (!item) {
        qWarning() << "Called with invalid row!" << currThumb;
        return false;
    }
    item->setData(true, LoadedRole);
    QString imageFileName = item->data(FileNameRole).toString();
    QImage thumb = loadThumbImage(imageFileName, thumbSize, Settings::thumbsLayout == Classic);

    if (!thumb.isNull()) {
        analyzeImage(imageFileName, thumb);

        if (Settings::exifThumbRotationEnabled) {
            imageViewer->rotateByExifRotation(thumb, imageFileName);
        }

        thumb = thumb.scaled(QSize(thumbSize, thumbSize), Settings::thumbsLayout == Classic ? Qt::KeepAspectRatio : Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);

        item->setData(qGray(thumb.scaled(1, 1, Qt::IgnoreAspectRatio, Qt::SmoothTransformation).pixel(0, 0)) / 255.0, BrightnessRole);

        if (Settings::thumbsLayout != Classic) {
            thumb = SmartCrop::crop(thumb, QSize(thumbSize, thumbSize));
        }

        item->setIcon(QPixmap::fromImage(thumb));
        item->setSizeHint(itemSizeHint());
    } else {
        item->setIcon(getErrorPixmap());
        return false;
    }
    return true;
}

QSize fixSize(QSize size) {
    if (size.width() <= 0 && size.height() <= 0) {
        return QSize();
    }
    if (size.width() <= 0) {
        size.setWidth(1);
    }
    if (size.height() <= 0) {
        size.setHeight(1);
    }
    return size;
}

QStandardItem * ThumbsViewer::addThumb(const QString &imageFullPath, qreal sortValue) {

    metadataCache->loadImageMetadata(imageFullPath);
    if (imageTags->dirFilteringActive && imageTags->isImageFilteredOut(imageFullPath)) {
        return nullptr;
    }

    QStandardItem *thumbItem = new QStandardItem();
    QSize currThumbSize;

    thumbFileInfo = QFileInfo(imageFullPath);
    thumbItem->setData(true, LoadedRole);
    thumbItem->setData(nameSortValues.value(imageFullPath), FileNameSortRole); // We can't know this yet, so this is best effort
    thumbItem->setData(thumbFileInfo.size(), SizeRole);
    thumbItem->setData(thumbFileInfo.lastModified(), TimeRole);
    thumbItem->setData(thumbFileInfo.suffix(), TypeRole);
    const QSize resolution = metadataCache->getResolution(imageFullPath);
    thumbItem->setData(resolution.width() * resolution.height(), ResolutionRole);
    thumbItem->setData(thumbFileInfo.filePath(), FileNameRole);
    if (Settings::thumbsLayout != Squares) {
        thumbItem->setTextAlignment(Qt::AlignTop | Qt::AlignHCenter);
        thumbItem->setText(thumbFileInfo.fileName());
    }
    thumbItem->setSizeHint(itemSizeHint());

    QImage thumb = loadThumbImage(imageFullPath, thumbSize, Settings::thumbsLayout == Classic);
    if (!thumb.isNull()) {
        analyzeImage(imageFullPath, thumb);

        if (Settings::exifThumbRotationEnabled) {
            imageViewer->rotateByExifRotation(thumb, imageFullPath);
        }

        thumb = thumb.scaled(QSize(thumbSize, thumbSize), Settings::thumbsLayout == Classic ? Qt::KeepAspectRatio : Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);

        if (Settings::thumbsLayout != Classic) {
            thumb = SmartCrop::crop(thumb, QSize(thumbSize, thumbSize));
        }

        thumbItem->setData(qGray(thumb.scaled(1, 1).pixel(0, 0)) / 255.0, BrightnessRole);

        thumbItem->setIcon(QPixmap::fromImage(thumb));
    } else {
        thumbItem->setIcon(getErrorPixmap());
        thumbItem->setSizeHint(itemSizeHint());
        currThumbSize.setHeight(BAD_IMAGE_SIZE);
        currThumbSize.setWidth(BAD_IMAGE_SIZE);
    }

    if (sortValue != -1) {
        int row = 0;
        for (row=0; row < thumbsViewerModel->rowCount(); row++) {
            if (thumbsViewerModel->index(row, 0).data(SortRole).toReal() > sortValue) {
                break;
            }
        }
        thumbsViewerModel->insertRow(row, thumbItem);
    } else {
        thumbsViewerModel->appendRow(thumbItem);
    }
    return thumbItem;
}

void ThumbsViewer::mousePressEvent(QMouseEvent *event) {
    QListView::mousePressEvent(event);

    if (Settings::reverseMouseBehavior && event->button() == Qt::MiddleButton) {
        if (selectionModel()->selectedIndexes().size() == 1)
                emit(doubleClicked(selectionModel()->selectedIndexes().first()));
    }
}

const QPixmap &ThumbsViewer::getErrorPixmap()
{
    if (errorPixmap.width() == thumbSize && errorPixmap.height() == thumbSize) {
        return errorPixmap;
    }
    errorPixmap = QPixmap(thumbSize, thumbSize);
    errorPixmap.fill(Qt::transparent);
    QPainter p(&errorPixmap);
    QRect iconRect(0, 0, BAD_IMAGE_SIZE, BAD_IMAGE_SIZE);
    iconRect.moveCenter(errorPixmap.rect().center());
    p.drawPixmap(iconRect.topLeft(), QIcon(":/images/error_image.png").pixmap(iconRect.size()));
    p.end();
    return errorPixmap;
}

void ThumbsViewer::invertSelection() {
    QItemSelection toggleSelection;
    QModelIndex firstIndex = thumbsViewerModel->index(0, 0);
    QModelIndex lastIndex = thumbsViewerModel->index(thumbsViewerModel->rowCount() - 1, 0);
    toggleSelection.select(firstIndex, lastIndex);
    selectionModel()->select(toggleSelection, QItemSelectionModel::Toggle);
}

void ThumbsViewer::setNeedToScroll(bool needToScroll) {
    this->isNeedToScroll = needToScroll;
}

void ThumbsViewer::setImageViewer(ImageViewer *imageViewer) {
    this->imageViewer = imageViewer;
}
